<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Products', function (Blueprint $table) {
            $table->increments('id');

            $table->text('features')->defaul("{}");
            $table->text('editorialreview');
            $table->integer('featured')->default(0);

            $table->string("name")->index();
            $table->string("gender")->index();
            $table->string("brand")->index();
           $table->string("category")->index();
           $table->string("color")->index()->nullable();
           $table->integer("price")->index();
           $table->string("imgurls")->index();
             $table->string("thumbnail")->index();
             $table->string("seo_slug")->index();

            $table->string("asin");

            $table->integer("views")->default("0");
            $table->integer("cardadded")->default("0");
            $table->float("bestoffer")->default("0");

            $table->integer("price_updates")->default("0");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Products');
    }
}
