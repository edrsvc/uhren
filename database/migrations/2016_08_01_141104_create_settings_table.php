<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->index();
            $table->string('description')->index();
            $table->string('keywords')->index();
            $table->string('robots')->index();
            $table->string('copyright')->index();
            $table->string('topic')->index();
            $table->string('revisit_after')->index();
            $table->string("google_analytics_code")->nullable();
            $table->string("google_site_verify_code")->nullable();
            $table->string("additional_javascript")->nullable();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Settings');
    }
}
