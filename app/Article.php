<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
        "seo_slug","category","text","description","title", "keywords","image_credit","image_organization", "image_caption"
    ];
    //
}
