<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $fillable = array(
        "title","google_adsense_code","google_site_verify_code",
        "additional_javascript","keywords","description",
        "revisit_after","copyright","robots",
        "title","google_analytics_code","google_site_verify_code","additional_javascript");
    protected $guarded  = array('_method', '_token');
}
