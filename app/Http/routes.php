<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/




use Illuminate\Http\Request;


Route::get('/', 'ShoppingController@index');

// ADMIN
Route::group(['middleware' => 'auth'], function () {
    Route::group(['middleware' => 'admin'], function () {
        Route::group(['prefix' => 'admin'], function () {

            // DASHBOARD
            Route::get('', function ()    {
                return view('admin.dashboard');
            });


            Route::group(['prefix' => 'site'], function () {
                Route::group(['prefix' => 'meta'], function () {
                    Route::get('', function ()    {
                        return view('admin.editmeta');
                    });
                    Route::patch('', "AdminController@editSettings");
                });
                Route::group(['prefix' => 'general'], function () {
                    Route::get('', function ()    {
                        return view('admin.general');
                    });
                    Route::patch('', "AdminController@editSettings");
                });

            });
            Route::group(['prefix' => 'products'], function () {
                Route::group(['prefix' => 'add'], function () {
                    Route::get('', function ()    {
                        return view('admin.addproduct');
                    });
                    Route::PUT("", "AdminController@saveNewProduct");
                });

                Route::post("/delete/{id}", "AdminController@removeProduct");

                Route::post("feature/{id}/{status}", function ($id, $status){
                        \App\Product::where("id", $id)->update(["featured"=> $status]);
                    return $status;
                });

                Route::get('/view', 'AdminController@viewProducts');
            });

            Route::group(["prefix"=>"blog"], function (){
                Route::get("/view", "BlogAdminController@viewBlogPOsts");
                Route::get("/add", "BlogAdminController@newBlogPost");
                Route::post("/add", "BlogAdminController@savePost");
                Route::post("/update/{id}", "BlogAdminController@updatePost");
                Route::get("/update/{id}", "BlogAdminController@updatePostView");
            });
        });

    });

});

Route::group(['prefix' => 'products'], function () { // post
    Route::post('/quickreview/{id}','ShoppingController@quickReview');
});

Route::group(['prefix' => 'shop'], function ($prefix) { // post

   # Route::get('zubeheor/find/{categories?}/{undercategory?}/{brands?}/{price?}/{colors?}', 'ShoppingController@renderCategoryPage');

    Route::get('{gender}/find/{categories?}/{brands?}/{price?}/{colors?}', 'ShoppingController@renderCategoryPage');

    Route::get('{gender}/v/{category}/{id}/{name}', 'ViewProductController@renderProductPage');

});

Route::group(['prefix' => 'visitor'], function ($prefix) { // post

    Route::group(['prefix' => 'edit'], function ($prefix) { // post
        Route::post("/show/{number}", "ShoppingController@editShowNumber");
        Route::post("/sort/{kind}", "ShoppingController@editSortBy");
        Route::post("/showkind/{kind}", function ($kind){
            if(in_array($kind, ["tile", "list"])){
                Request()->session()->put("showkind", $kind );
            } else {
                return false;
            }

        });
    });

    /**
     * Cart functionallity
     */

});
/**
 * Cart functionallity
 */

Route::group(['prefix' => 'cart'], function ($prefix) { // post
    Route::post("add/{asin}/{qty}", "ShoppingCartController@addProductToCart");
    Route::post("delete/{asin}", "ShoppingCartController@deleteProductFromCart");
});
Route::get("cart", "ShoppingCartController@renderShoppingCart");
Route::get("datenschutzbestimmungen", function (){
    return view("privacy");
});
Route::get("impressum", function (){
    return view("impressum");
});

Route::get("search", function (){
    return view("searchresult");
});

Route::group(['prefix' => 'blog'], function ($prefix) { // post
    Route::get("/", "BlogController@index");
    Route::get("/view/{id}/{slug}", "BlogController@viewArticle");
});




Route::auth();

Route::get('/home', 'HomeController@index');

/*
 * SITEMAP
 */

Route::get('sitemap', function() {
    $sitemap = App::make("sitemap");
    $sitemap->setCache('laravel.sitemap', 60);

    // check if there is cached sitemap and build new only if is not
    if (!$sitemap->isCached())
    {
        // Ad products!
        $products = \App\Product::get()->all();
        $time = \Carbon\Carbon::now();
        $sitemap->add(URL::to("/"), $time, '1.0', 'weekly');

        $sitemap->add(URL::to("shop/damen/find"), $time, '0.9', 'daily');
        $sitemap->add(URL::to("shop/damen/find/automatikuhren"), $time, '0.8', 'daily');
        $sitemap->add(URL::to("shop/damen/find/quarzuhren"), $time, '0.8', 'daily');
        $sitemap->add(URL::to("shop/damen/find/digitaluhren"), $time, '0.8', 'daily');

        $sitemap->add(URL::to("shop/herren/find"), $time, '0.9', 'daily');
        $sitemap->add(URL::to("shop/herren/find/automatikuhren"), $time, '0.8', 'daily');
        $sitemap->add(URL::to("shop/herren/find/quarzuhren"), $time, '0.8', 'daily');
        $sitemap->add(URL::to("shop/herren/find/digitaluhren"), $time, '0.8', 'daily');

        $sitemap->add(URL::to("shop/unisex/find/smartwatches"), $time, '0.9', 'daily');

        $sitemap->add(URL::to("shop/haushaltsuhren/find"), $time, '0.9', 'daily');
        $sitemap->add(URL::to("shop/haushaltsuhren/find/kuckucksuhren"), $time, '0.8', 'daily');
        $sitemap->add(URL::to("shop/haushaltsuhren/find/wanduhren"), $time, '0.8', 'daily');
        $sitemap->add(URL::to("shop/haushaltsuhren/find/pendeluhren"), $time, '0.8', 'daily');
        $sitemap->add(URL::to("shop/haushaltsuhren/find/kurzzeitwecker"), $time, '0.8', 'daily');
        $sitemap->add(URL::to("shop/haushaltsuhren/find/haushaltswecker"), $time, '0.8', 'daily');

        $sitemap->add(URL::to("shop/zubeheor/find/uhrenarmbaender"), $time, '0.9', 'daily');
        $sitemap->add(URL::to("shop/reperatur/find/reperatursets"), $time, '0.9', 'daily');
        $sitemap->add(URL::to("shop/aufbewahrung/find/uhrenbeweger"), $time, '0.9', 'daily');
        $sitemap->add(URL::to("shop/aufbewahrung/find/uhrenboxen"), $time, '0.9', 'daily');
        $sitemap->add(URL::to("shop/batterien/find/uhrenbatterien"), $time, '0.9', 'daily');
        $sitemap->add(URL::to("blog"), $time, '1.0', 'daily');

        foreach ($products AS $product)
        {
            $sitemap->add(URL::to("shop/".$product->gender."/v/".$product->category."/".$product->id."/".$product->seo_slug),
                 $product->updated_at, "0.8", "daily");
        }
        // Add Blog Posts
        $articles = \App\Article::get()->all();
        foreach ($articles AS $article)
        {
            $sitemap->add(URL::to("blog/view/".$article->id."/".$article->seo_slug),
                $product->updated_at, "0.8", "daily");
        }

    }

    return $sitemap->render('xml');
});