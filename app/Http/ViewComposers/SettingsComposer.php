<?php

namespace App\Http\ViewComposers;
use App\Http\Controllers\ShoppingCartController;
use App\Http\Controllers\ShoppingController;
use App\Http\Requests\Request;
use Illuminate\View\View;
use App\Settings;

/**
 *
 */
class SettingsComposer
{
    public function __construct()
    {
    }

    public function compose(View $view){
        # dd($this->settings->all()[0]);



        $view->with("settings", \Cache::remember('settings', 10, function() {
            return Settings::all();
        }));

        $products = ShoppingCartController::getCartProducts();
       # dd($products);
        $sessionProducts = \Request::session()->get("shopping_cart");




        // Calculate SUBTOTAL!
        $subtotal = 0;
        $products->map(function($price) use(&$subtotal, $sessionProducts){
            $subtotal = $subtotal + ($price->price * $sessionProducts[$price->asin]["qty"]);
        });
        // Counting the stuff
        if( \Request::session()->has("shopping_cart"))
        {
            $view->with("shopping_count", count($sessionProducts) );
            $view->with("cart_items", $products);
            $view->with("subtotal", $subtotal);
        }
        else{
            $view->with("shopping_count", "");
        }





    }





}