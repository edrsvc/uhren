<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class SessionController extends Controller
{ // !!!!OMG
    /**
     * @param $request Request
     * @param $viewed string ASIN - The ASIN what the visitor visited
     */
    static function viewedProducts($request, $viewed)
    {
        // lets see if the session already has the viewed product array
        if (session()->has("viewed_products"))
        {
            $visited = session()->get("viewed_products");
            // Is it already in the ARRAY ?
            if(in_array($viewed, $visited))
            {
                return;
            } else {
                // NO, so lets add it!
                $visited[] = $viewed;
                $visited = array_slice($visited,-15);
                $request->session()->forget('viewed_products');
                $request->session()->put("viewed_products", $visited);
            }
        } else {
            // No.. lets create it!
            $request->session()->push("viewed_products", $viewed);
        }
    }

    /**#
     * Adds a new product to the session!
     * @param $request Request
     * @param $product string - ASIN
     * @param $qty Integer Product qty
     */
    static function addTOCart($request, $product, $qty, $CartItemId)
    {
        if($request->session()->has("shopping_cart"))
        {
            $products = $request->session()->get("shopping_cart");

            // Is the ASIN in the array??
            if(array_key_exists($product,$products))
            {

                $products[$product] =  ["qty" => (string)$qty, "CartItemId" => (string)$CartItemId];

            }
            else{
                $products[$product] =  ["qty" => (string)$qty, "CartItemId" => (string)$CartItemId];
            }

            $request->session()->forget("shopping_cart");
            $request->session()->put("shopping_cart", $products);

        }
        else{

            $request->session()->put("shopping_cart.".$product, ["qty"=>$qty, "CartItemId" => (string)$CartItemId[0]]);
        }
        #dd($request->session()->all());
    }

    /**
     * Deletes the item by ASIN from SESSION
     * @param $asin string
     */
    static function deleteCartItem($asin){

        \Request::session()->forget("shopping_cart.".$asin);

    }
}