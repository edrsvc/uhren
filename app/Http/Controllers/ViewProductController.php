<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

use App\Http\Requests;
use Mockery\CountValidator\Exception;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ViewProductController extends Controller
{
    public function renderProductPage(Request $request,$gender, $category, $id)
    {
        $product =  Product::select("asin","gender","name","brand","category","thumbnail","imgurls","editorialreview")->where("id", $id);


        #if(count($product))
        /*
        if(!isset($product)){
            return view("errors.404");
        }
        */

        $product->increment("views");

       $product = $product->get()->first();

        if($product == NULL){
            throw new NotFoundHttpException();
        }

        // Doing some session stuff ...
        SessionController::viewedProducts($request,$product->asin);
        // Done

        $amazon = json_decode(\Cache::remember("amzProduct_".$id, 20, function() use ($product)
        {
            return json_encode(ShoppingController::amazonLookup($product->asin, array('Variations', 'OfferFull' )));
        }));


        #$amazon =

            #ShoppingController::amazonLookup($product->asin, array('Variations', 'OfferFull' ));

        if($amazon === FALSE)
        {
            return redirect()->back()->withErrors('Error while trying to lookup the product. Is the ASIN correct?');
        }

        if(isset($amazon->Items->Item->Variations) )
        {
            $hasVariations = ShoppingController::validateVariations($amazon->Items->Item->Variations);
        }
        else{
            $hasVariations = FALSE;
        }


        // Creating the first elements
        $productArray = array(
            "asin" => $product->asin,
            "gender" => $product->gender,
            "name" => $product->name,
            "brand" => $product->brand,
            "category" => $product->category,
            "thumbnail" =>  $product->thumbnail,
            "imgurls" => explode(',',$product->imgurls),
            "editorreview" => $product->editorialreview,
            "thumbnail" => $product->thumbnail,

            "features" => json_decode( \Cache::remember($id, 0, function() use ($id) {
                #  Product::where('id', $productId)->select("features")->get()[0]
                    return (Product::where('id', $id)->select("features")->get()->first()["features"]);
            }))

        );

        if($hasVariations) {
            ShoppingController::updatePrice($product, $amazon->Items->Item->Variations->Item->Offers->Offer->OfferListing->Price->Amount);
            $productArray['price'] = $amazon->Items->Item->Variations->Item->Offers->Offer->OfferListing->Price->Amount;

        } else {
            ShoppingController::updatePrice($product, $amazon->Items->Item->OfferSummary->LowestNewPrice->Amount);
            try {
                $productArray['price'] = $amazon->Items->Item->OfferSummary->LowestNewPrice->Amount;
            } catch (Exception $e){
                return redirect()->Back()->withErrors("Das Produkt ist im Moment leider nicht verfügbar.");
            }


        }

        $recommendations = ShoppingController::getLatestViewedProducts();

        $meta = $this->compileMeta($productArray);

        return view("viewproduct", compact('productArray', 'recommendations', 'meta'));
    }
    private function compileMeta($productArray){
        $meta = array();
        $t_len = strlen($productArray["name"]);
        $d_len = 166;
        $c = strlen($productArray["editorreview"]);
        $add = $c/$d_len;
        if(count($productArray["editorreview"]) > 0)
        {

            if($add < 1) {

                $k = 1 - $add;
                $num = $k * $d_len;

                if($t_len >= $num) {

                    $meta["description"] = strip_tags($productArray["editorreview"])." ".substr($productArray["name"],0,$num);

                } else {
                    $meta["description"] = ucfirst($productArray["gender"])." bei uhren123. Hochwertige ".$productArray["brand"]." ".$productArray["category"]. " günstig.";
                }


            } else {

                $meta["description"] = substr(strip_tags($productArray["editorreview"]),0,157)."..";
            }

        } else {
            if($add < 1) {
                $k = 1 - $add;
                $num = $k * $d_len;

                if($t_len >= $num) {

                    $meta["description"] = strip_tags($productArray["editorreview"])." ".substr($productArray["name"],0,$num);

                } else {
                    $meta["description"] = ucfirst($productArray["gender"])." bei uhren123. Hochwertige ".$productArray["brand"]." ".$productArray["category"]. " günstig.";
                }


            } else {

                $meta["description"] = substr(strip_tags($productArray["editorreview"]),0,157)."..";
            }
        }

        $meta["title"] = $productArray["name"];

        $meta["keywords"] = $productArray["gender"].", ".$productArray["brand"].", ".$productArray["category"].", uhren123";

        return $meta;

    }
}
