<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use ApaiIO\Configuration\GenericConfiguration;
use ApaiIO\Operations\Lookup;
use ApaiIO\ApaiIO;
use App\Http\Requests;
use Mockery\CountValidator\Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AdminController extends Controller
{
    protected $errors = array();

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|string
     */
    public function saveNewProduct(Request $request)
    {
        // Variable to store if the products hast variations
        $hasVariations = false;

        // Validate inputs... before we begin API procedure
        $this->validate($request,[
            "gender" => "required",
            "asin" => "required|min:10|max:10",
            "category" => "required",
        ]);

        // Ceck if the product already exists in DB
        $check = Product::where("asin", $request->asin)->get();
        if(count($check) >= 1) {
            return redirect()->back()->withErrors('Error the product is already in DB');
        }


        // API Request... returns FALSE if error
        $amazon = ShoppingController::amazonLookup($request->get("asin"),
            array('Images', 'Large','VariationImages', 'Variations', 'OfferFull' ));
        // We got an ERROR?
        if($amazon === FALSE)
        {
            return redirect()->back()->withErrors('Error while trying to lookup the product. Is the ASIN correct?');
        }


        // Checking for VARIATIONS ....
        if(isset($amazon->Items->Item->Variations)){
            $hasVariations = true; // Yes - we have variations

            // Checking if there are MORE than 1 variation ....
            // This will be an ERROR, we are not prepared for that, TODO!
            try {
                if($amazon->Items->Item->Variations->TotalVariations > 1){
                    throw new HttpException("Troubles while retrieving product variations!");
                }
            } catch(HttpException $e) {
                // Catch the ERROR... redirect back
                return redirect()->back()->withErrors($e);
            }

        }
        // Create new PRODUCT && asign attributes
        try{

            $product = new \App\Product;
            // ====== Admin inputs -> Save the inputs of the submit form
            $product->gender = $request->get("gender");
            $product->category = $request->get("category");
            // ====== Api responses -> work with the API response
            $product->name  = $amazon->Items->Item->ItemAttributes->Title;

            $product->features = json_encode($amazon->Items->Item->ItemAttributes->Feature);

               #dd($amazon->Items->Item->ItemAttributes->Feature);
            // Delete BLANK spacers in the title and replace it with a dash " - "
            $product->seo_slug = str_replace(array(" ", "/","\\"),"-",$amazon->Items->Item->ItemAttributes->Title);

            $product->brand = $amazon->Items->Item->ItemAttributes->Brand;
            $product->asin  = $amazon->Items->Item->ASIN;
            // Does product have a editorial review?
            if(isset($amazon->Items->Item->EditorialReviews->EditorialReview->Content))
            {
                $product->editorialreview = $amazon->Items->Item->EditorialReviews->EditorialReview->Content;
            }


            if($hasVariations){


                // Get FIRST variation and assign it!
                $product->color = $amazon->Items->Item->Variations->Item->ItemAttributes->Color;
                $product->price =
                    $amazon->Items->Item->Variations->Item->Offers->Offer->OfferListing->Price->Amount;

               # $product->amountsaved = // OLD
               # $amazon->Items->Item->Variations->Item->Offers->Offer->OfferListing->AmountSaved->Amount;

                // Thumbnail
                $thumb = (string)$amazon->Items->Item->SwatchImage->URL;
                $pos = strpos($thumb, "_");
                $trimedImg = substr($thumb,0,$pos );
                $thumb = str_replace("http://ecx.images-amazon.com/","https://images-eu.ssl-images-amazon.com/",$trimedImg);


                $product->thumbnail = $thumb;
                // Indexing IMAGES ! == Creating a CSV list of the imagesetz

                // Refference to the imagesets in the DOM REST
                $images = $amazon->Items->Item->Variations->Item->ImageSets->ImageSet;
                // Refference for the images to be save on the DB
                $dbImages = array();
                #
                #dd($images);
                foreach($images AS $image)
                {

                    $image = $image->LargeImage->URL;
                    $pos = strpos($image, "_");
                    $timedImg = substr($image,$pos, -1 );
                    $fin =  str_replace("http://ecx.images-amazon.com/","https://images-eu.ssl-images-amazon.com/", $timedImg);
                    $dbImages[] = $fin;
                }

                $product->imgurls = implode(",", $dbImages);

                // There are NO veriations
            } else {

                // No veriation ... easy going...
                $product->color = $amazon->Items->Item->ItemAttributes->Color;
                $product->price = $amazon->Items->Item->OfferSummary->LowestNewPrice->Amount;

                // Thumbnail!
                $thumb = (string)$amazon->Items->Item->MediumImage->URL;
                $pos = strpos($thumb, "_");
                $trimedImg = substr($thumb,0,$pos );
                $thumb = str_replace("http://ecx.images-amazon.com/","https://images-eu.ssl-images-amazon.com/",$trimedImg);
                $product->thumbnail = $thumb;


                // Check if IMAGES available!
                $images = $amazon->Items->Item->ImageSets->ImageSet;
                if(count($images) <= 0){
                    // Error!
                    return redirect()->back()->withErrors('Error while trying to fetch product images.');
                }
                // Create URL to store the images
                $dbImages = array();

                // Loop throught the images
                foreach($images AS $image) {
                    // Get image!
                    $image = (string)$image->SwatchImage->URL;
                    // Delete image SIZE && .JPG
                    $pos = strpos($image, "_");
                    $trimedImg = substr($image,0,$pos );
                    // SSL!
                    $fin =  str_replace("http://ecx.images-amazon.com/","https://images-eu.ssl-images-amazon.com/",$trimedImg);
                    $dbImages[] = $fin;
                }

                $product->imgurls = implode(",", $dbImages);




                #$product->amountsaved =
                 #   $amazon->Items->Item->Offers->Offer->OfferListing->AmountSaved->Amount;
            }
            // Do the save!
            $product->save();

        } catch (\QueryException $e){
            dd($e);
            return redirect()->back()->withErrors("2 Error while trying to insert the product to the database.");
        } catch (\PDOException $e) {
               dd($e);
                return redirect()->back()->withErrors("1 Error while trying insert the product to the database.");
        }

        $request->session()->flash("notification", "Product added successfully!");
        return redirect()->back();

    }

    public function viewProducts(){
        $products = \App\Product::orderBy("views", "DESC")->paginate(50);
        return view('admin.viewproducts', compact('products'));
    }
    public function removeProduct(Request $request, $id)
    {
        try{
            $product = \App\Product::where("id", $id)->first();
            $product->delete();
            return "Success";
        } catch ( \Illuminate\Database\QueryException $e ) {
            return $e;
        }
    }
    public function editSettings(Request $request)
    {
        try {
            $settings = \App\Settings::where("id", 1)->get()->first();
            $settings->update($request->all());
            $request->session()->flash("notification", "Meta updates successfully!");
            return redirect()->back();

        } catch (\PDOException $e) {
            return redirect()->back()->withErrors('Error while trying to update settings.');
        }
    }
}
