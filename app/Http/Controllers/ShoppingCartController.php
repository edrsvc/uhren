<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

use App\Http\Requests;



use ApaiIO\Configuration\GenericConfiguration;
use ApaiIO\Operations\Lookup;
use ApaiIO\Operations\Search;
use ApaiIO\ApaiIO;
use ApaiIO\Operations\CartAdd;
use ApaiIO\Operations\CartCreate;
use ApaiIO\Operations\ModifyCart;

class ShoppingCartController extends Controller
{
    public function addProductToCart(Request $request)
    {
        // Add the product to the cart!

        #  dd($request->session()->get("shopping_cart"));


        // Defining references
        $conf = new GenericConfiguration();
        $client = new \GuzzleHttp\Client();
        $req = new \ApaiIO\Request\GuzzleRequest($client);

        $session_cart =  $request->session()->get("shopping_cart");


        // Create new apai settings instance!
        $conf
            ->setCountry('de')
            ->setAccessKey('AKIAICDYNSE7IPBYDKHA')
            ->setSecretKey('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')
            ->setAssociateTag('nomikcon-21')
            ->setRequest($req);
        $apaiIO = new ApaiIO($conf);

        // Amazon Carts!
        if(!$request->session()->has("amz_cart")){
            // Cart not created yet, let's create one!
            $cartCreate = new CartCreate();

            $cartCreate->addItem($request->asin, $request->qty);
            $response = $apaiIO->runOperation($cartCreate);
            $xml = simplexml_load_string($response);

            if(isset($xml->Cart->Request->Errors->Error)){
                if($xml->Cart->Request->Errors->Error->Code == "AWS.ECommerceService.ItemNotEligibleForCart")
                {
                    \Session::flash("notification", "Entschuldigen Sie, aber das Produkt ist leider vergriffen.");
                    return;
                }

            }

            #$request->session(["amz_cart" => $xml->Cart->CartId]); HrCVfQ0Gq1HpcGDwctFHjj7sznI=
            $request->session()->put("amz_cart",  (string)$xml->Cart->CartId[0]);
            $request->session()->put("amz_hmac",  (string)$xml->Cart->HMAC[0]);
            $request->session()->put("amz_pruchase_url",  (string)$xml->Cart->PurchaseURL[0]);

            #dd($request->session()->all());
            #dd($xml);
        }
        else{
            // If the Product is _ALREADY_ on the cart, we update QTY

            if(isset($session_cart[$request->asin]))
            {

                $cartAdd = new ModifyCart();

                $cartAdd->setCartItemId($session_cart[$request->asin]["CartItemId"]);

                $cartAdd->setCartId($request->session()->get("amz_cart"));
                $cartAdd->setHMAC($request->session()->get("amz_hmac"));
                $cartAdd->setResponseGroup(["Request","Cart","CartSimilarities","CartNewReleases","CartTopSellers"]);
                $cartAdd->modifyItem( $request->qty);
            }
            else{
                // it doesnt exits so..
                // Add product to existing CART
                $cartAdd = new CartAdd();
                $cartAdd->setCartId($request->session()->get("amz_cart"));
                $cartAdd->setHMAC($request->session()->get("amz_hmac"));
                $cartAdd->addItem($request->asin, $request->qty);

            }
            $response = $apaiIO->runOperation($cartAdd);
            $xml = simplexml_load_string($response);


            if(isset($xml->Cart->Request->Errors->Error)){

                if($xml->Cart->Request->Errors->Error->Code == "AWS.ECommerceService.ItemNotEligibleForCart")
                {
                    \Session::flash("notification", "Entschuldigen Sie, aber das Produkt ist leider vergriffen.");
                    return;
                }

            }

        }


        if(isset($cartAdd) && get_class($cartAdd) == "ApaiIO\Operations\ModifyCart")
        {
            SessionController::addTOCart($request,$request->asin, $request->qty, $session_cart[$request->asin]["CartItemId"]);
        }
        else{

            SessionController::addTOCart($request,$request->asin, $request->qty, $xml->Cart->CartItems->CartItem[0]->CartItemId[0]);

        }


    }

    /**
     * Add a new Item
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function renderShoppingCart(Request $request)
    {
        $products = static::getCartProducts();
        return view("cart",compact("products"));
    }









    /*
     * Delete an intem
     */
    public function deleteProductFromCart(Request $request)
    {
        $conf = new GenericConfiguration();
        $client = new \GuzzleHttp\Client();
        $req = new \ApaiIO\Request\GuzzleRequest($client);

        $conf
            ->setCountry('de')
            ->setAccessKey('AKIAICDYNSE7IPBYDKHA')
            ->setSecretKey('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')
            ->setAssociateTag('nomikcon-21')
            ->setRequest($req);
        $apaiIO = new ApaiIO($conf);
        $cartAdd = new ModifyCart();

        $cartAdd->setCartItemId($request->session()->get("shopping_cart")[$request->asin]["CartItemId"]);

        $cartAdd->setCartId($request->session()->get("amz_cart"));
        $cartAdd->setHMAC($request->session()->get("amz_hmac"));
        $cartAdd->modifyItem(0);

        $response = $apaiIO->runOperation($cartAdd);

        $xml = simplexml_load_string($response);
        /*
        print_r("<pre>");
        print_r($xml);
        print_r("</pre>");
        */
        SessionController::deleteCartItem($request->asin);

    }

    /**
     * Get cart-added products
     * @return mixed
     */
    static function getCartProducts(){

        $asins = \Request::session()->get("shopping_cart");

        $products = Product::where(function ($query) use($asins)
        {
            if(count($asins)>=1){
                foreach ($asins as $asin => $qty)
                {
                    $query->orWhere("asin", $asin );
                }
            }
            else{
                $query->Where("asin", $asins);
            }

        })
        ->get();
        return $products;

    }


}
