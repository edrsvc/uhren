<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use ApaiIO\Configuration\GenericConfiguration;
use ApaiIO\Operations\Lookup;
use ApaiIO\Operations\Search;
use ApaiIO\ApaiIO;
use App\Http\Requests;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ShoppingController
 * @package App\Http\Controllers
 */
class ShoppingController extends Controller
{
    static function createAmzObject()
    {

    }
    public function index()
    {
        $featuredProducts = Product::where('featured', 1)->get()->all();
        $latestProducts = Product::orderBy('created_at', 'asc')->take(10)->get();
        return view('welcome', compact('featuredProducts'), compact('latestProducts'));
    }

    /**
     * @param $asin
     * @param $responseGroups
     * @return bool|\SimpleXMLElement
     */
    static function amazonLookup($asin, $responseGroups)
    {
        $conf = new GenericConfiguration();
        $client = new \GuzzleHttp\Client();
        $req = new \ApaiIO\Request\GuzzleRequest($client);

        $conf
            ->setCountry('de')
            ->setAccessKey('AKIAICDYNSE7IPBYDKHA')
            ->setSecretKey('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')
            ->setAssociateTag('nomikcon-21')
            ->setRequest($req);
        $apaiIO = new ApaiIO($conf);
        $search = new Lookup();
        $search->setCondition('All');
        $search->setItemId($asin);
        $search->setResponseGroup($responseGroups);

        $formattedResponse = $apaiIO->runOperation($search);
        $xml = simplexml_load_string($formattedResponse);
        # dd($xml);

        // Check if the request had errors
        if(isset($xml->Items->Request->Errors->Error->Message)){
            return false;
        } else {
            return $xml;
        }

    }


    static function validateVariations($variations = FALSE)
    {
        if($variations)
        {
            return true;
        }
        else{
            return false;
        }

    }

    /**
     * Update the PRICE in DB!
     * @param $product
     * @param $price
     * @return
     */
    static function updatePrice($product, $price){
        if($product->price != $price)
        {
            $product->price = $price;
            $product->increment("price_updates");
            $product->save();
            return true;
        }
        else{
            return;
        }

    }
    /**
     * @param Request $request
     * @param $productId
     * @return $this|string
     */
    public function quickReview($productId)
    {
        return \Cache::remember($productId, 0, function() use ($productId)
        {
            $j = (Product::where('id', $productId)->select("features")->get()->first()["features"]);
            if($j[1] == "}"){
                return '{ "0":"Features nicht verfügbar" }';
            }
            return (Product::where('id', $productId)->select("features")->get()->first()["features"]);
        });
    }

    static function getLatestViewedProducts(){

        $viewArray = array();

        if(session()->get("viewed_products") === NULL)
        {
            return FALSE;
        }
        foreach(session()->get("viewed_products") as $key => $value )
        {
            $viewArray[] = $value;
        }

        // Fetch some other products ....

        $products = \DB::table("Products")->select("name","color","id", "price", "thumbnail", "gender","category", "seo_slug")
            ->where(function ($query) use ($viewArray)
        {
            foreach ($viewArray AS $asin)
            {
                $query->orWhere("asin", $asin);
            }
        })->take(10)->get();
        // Fetch some other products ....

        $products += \DB::table("Products")->select("name","id", "price", "thumbnail", "gender","category", "seo_slug")
            ->where(function ($query) use ($products)
        {
            foreach ($products AS $relation)
            {

                $query->orWhere("asin", 'LIKE', $relation->name);
                $query->orWhere("color", 'LIKE', $relation->color);
                $query->Where("gender", 'LIKE', $relation->gender);
            }
        })
            ->take(10)->get();
        return $products;
    }

    /**
     * @param Request $request
     * @param $gender
     * @return string
     */
    public function renderCategoryPage(Request $request, $gender, $cat = NULL, $brand = NULL)
    {


        if(!\Cache::has("gender"))
        {
            \Cache::put("gender",  array("damen" => 0, "herren" => 0,"unisex" =>0));
        }
        if(!\Cache::has("sortby"))
        {
            \Cache::put("sortby",  array("price desc","price asc","popularity desc","popularity asc"));
        }



        // Generating the ASIN array from SESSION
        // Check if the visitor is _NOT_ looking for human wearables
        if(!isset(\Cache::get("gender")[$gender]))
        {
            // Check if the category has been set
            if($cat !== NULL)
            {
                $categories = $cat;
            }
            else{

                // No specific category set?
                // Lets make that it loads ALL
                $cat = FALSE;
            }


            $search = $this->compileSearchField($gender, $cat);


        }
        else{
            if($cat !== NULL)
            {
                $categories = $cat;
                $search = $this->compileSearchField($gender,$cat);
            }
            else{

                // No specific category set?
                // Lets make that it loads ALL

                $cat = FALSE;
                $search = $this->compileSearchField($gender);
            }

        }

        // Lets see if there is a category set in the request...
        // TODO: Is this really needed?

        if(!isset($request->categories))
        {   // No there isn't a category, lets see if the visitor visits a category

            if(isset($categories) && $categories !== NULL){
                $category = $categories;
            }

            else{
                $category = $search["categories"];

            }
        }
        else{
            $category =  $request->categories;

            # dd($category);
        }

        #dd($category);
        // Brands
        if(!isset($request->brands)){

            $brands = $search["brands"];

        }
        else{
            if($brand !== NULL)
            {
                $brands = $brand;

            } else {
                $brands = $request->brands;
            }


        }

        // Checking if the user had choosen a custom number of products per page ...
        if($request->session()->has("show"))
        {
            // Did he want to see ALL products?
            if($request->session()->get("show") !== "all")
            {

                $showPaginate = $request->session()->get("show");
            }
            else{
                // Limit it to 30.
                $showPaginate = 30;
            }
        }
        else {
            // .. Everything normal...
            $showPaginate = 12;
        }

        // Checking for sorting...
        if($request->session()->has("sort_by"))
        {
            $sortBy = $request->session()->get("sort_by");

            if(!isset(\Cache::get("sortby")[$sortBy])){

                $sortBy = array("","");

            } else {
                $sortBy = explode(" ",  $request->session()->get("sort_by"));
                if($sortBy[0] =="popularity" ){
                    $sortBy[0] == "views";
                }
            }
        }
        else {
            // .. Everything normal...
            $sortBy =  array("","");
        }

        // Get REcommendet products
        $recommendations = $this->getLatestViewedProducts();

        $products =  $this->getProducts($showPaginate,$sortBy,strtolower($gender), $brands, $category, $request->colors, $request->price);
        if(!$products) {
            throw new NotFoundHttpException();        }
        $request->flash();
        return view("kategorie", compact('search', 'products','recommendations'))->withInput($request);

    }


    /**
     * @param array $gender
     * @param array $brand
     * @param array $category
     * @param array $color
     * @return mixed
     */
    public function getProducts($showPaginate, $sortBy, $gender = array(), $brand = array() , $category = array(),$color = array(),$price = array())
    {


        $products = Product::where('gender', $gender)


            // == Brand ==
            ->where(function ($query) use ($brand)
            {
                    if(count($brand) == 1)
                    {
                        $query->orWhere('brand', 'LIKE', $brand);
                    } else if( count($brand) < 0 ){
                        foreach ($brand as $b) {

                            $query->orWhere('brand', 'LIKE', $b);
                        }
                    } else {
                        return;
                    }
            })
            // == Color ==
            ->where(function ($query) use ($color)
            {


                If(!isset($color))
                {
                    return;
                }
                foreach ($color as $c)
                {
                    $query->orWhere('color', 'LIKE', $c);
                }
                $query->orWhere('color', '');
            })
            // == Category ==
            ->where(function ($query) use ($category)
            {
                If(!isset($category))
                {
                    return;
                }


                // Check is we got an array
                if(is_string($category))
                {

                    $query->orWhere('category', ucfirst($category));
                }
                else{
                    // Its an array!
                    foreach ($category as $c)
                    {
                        $query->orWhere('category', $c);
                    }
                }

            })
            // == Price ==
            ->where(function ($query) use ($price)
            {
                if(isset($price["lower"]) && isset($price["upper"]))
                {
                    $query->whereBetween('price', array(str_replace(".", "",$price["lower"]),str_replace(".", "",$price["upper"])));

                }
                else{
                    return;
                }

            })
            ->orderBy($sortBy[0],$sortBy[1])
            ->paginate($showPaginate); // Pagination
        # dd($products);

        // Nothing found? 404!
        if(!count($products))
        {
            return FALSE;

        }


        return $products;
    }
    /**
     * Compiles the search-field!
     * @param $gender
     * @return bool
     */
    public function compileSearchField($gender, $category = null){

        // Crating vals
        $search = array(
          "gender" => $gender, "brands" => array(), "color" => array(), "categories" => array()
        );

        // Checking if category is given
        if($category === false){
            // Yes - Category is given!

            $products = \DB::select("  SELECT A.brand,A.color,A.category, B.maxprice,B.minprice
                                        FROM (SELECT * from Products 
                                        WHERE gender = :gender 
                                        GROUP BY brand, color, category) as A
                                        CROSS JOIN(SELECT max(price) as maxprice, min(price) as minprice FROM Products WHERE gender = :gender ) as B
										
										",
                ["gender" => strtolower($gender)]);

        }
        else{

            // Yes It's here ! So look for category too!
            $products = \DB::select("  SELECT A.brand,A.color,A.category,B.minprice,B.maxprice 
                                        FROM (SELECT * from Products 
                                        WHERE gender = :gender AND category = :category
                                        GROUP BY brand, color, category) as A
                                        CROSS JOIN(SELECT max(price) as maxprice, min(price) AS minprice  FROM Products WHERE gender = :gender AND category = :category) as B",
                ["category" => ucfirst($category), "gender" => strtolower($gender)]);
        }

        // No Products, No Search
        if(count($products) === 0){
               # dd("HI");
            return false;
        }

        // Max & Minprices!
        $search["minprice"] = $products[0]->minprice;
        $search["maxprice"] = $products[0]->maxprice;

        // Fill the searchfield arrays!
        foreach($products as $product)
        {
            if(!isset($search["colors"][ucfirst(strtolower($product->color))]) && $product->color != NULL)
            {
                $s = ucfirst(strtolower($product->color));
                $search["colors"][$s] = $s;
            }
            if(!isset($search["brands"][ucfirst(strtolower($product->brand))]))
            {
                $s = ucfirst(strtolower($product->brand));
                $search["brands"][$s] = $s;
            }
            if(!isset( $search["categories"][ucfirst(strtolower($product->category))] ))
            {
                $s = ucfirst(strtolower($product->category));
                $search["categories"][$s] = $s;
            }
        }
        // Bye!
        return $search;
    }

    /**
     * Updateing the visitors session settings!
     * @param Request $request
     * @param $number
     */
    public function editShowNumber(Request $request, $number){
        // Validate
        if(!in_array($number,array("6","15","18","all"))){
            return;
        }else{
            $request->session()->put("show", $number);
        }

    }

    public function editSortBy(Request $request, $sort){
        // Validate ..
        if(!in_array($sort,array("price desc","price asc","popularity desc","popularity asc"))){
            return;
        }else{
            $request->session()->put("sort_by", $sort);
        }


    }
}
