<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;

use App\Http\Requests;

class BlogController extends Controller
{
    public function index()
    {
        $articles = Article::orderBy("created_at","desc")->paginate(6);
        $latest = Article::latest();
        return view("blog.blogindex", compact("articles", "latest"));
    }

    public function viewArticle($id)
    {

        $article = Article::where("id", $id)->get()->first();
        $imgsize = getimagesize("uploads/".$article->article_image);

        return view("blog.viewarticle", compact("article","imgsize"));
    }
}
