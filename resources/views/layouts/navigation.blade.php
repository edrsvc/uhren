<div class="col-md-3">
    <ul class="list-group sidebar-nav-v1" id="sidebar-nav">
        <!-- Site Settings -->
        <li class="list-group-item ">
            <a>Site Settings</a>
            <ul >
                <li><a href="{{url("admin/site/general")}}">General Settings</a></li>
                <li><a href="{{url("admin/site/meta")}}">Meta Configurations</a></li>
            </ul>
        </li>
        <!-- Site Settings -->

        <!-- Site Settings -->
        <li class="list-group-item ">
            <a >Amazon</a>
            <ul >
                <li><a href="{{url("admin/products/view")}}">View Products</a></li>
                <li><a href="{{url("admin/products/add")}}">Add Product</a></li>
            </ul>
        </li>
        <!-- Site Settings -->

        <!-- Site Settings -->
        <li class="list-group-item ">
            <a >Blog</a>
            <ul>
                <li><a href="{{url("admin/blog/view")}}">View Articles</a></li>
                <li><a href="{{url("admin/blog/add")}}">New Article</a></li>
            </ul>
        </li>
        <!-- Site Settings -->
    </ul>
</div>