<!DOCTYPE html>
<html lang="de">
<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="msvalidate.01" content="7C10AEBAA0FAFF26C7AD9F23AE447C5D" /> 

    <meta name="copyright" content="{{$settings[0]->copyright}}">
    <meta name="robots" content="{{$settings[0]->robots}}">
    <meta name="topic" content="{{$settings[0]->topic}}">


    <link rel="icon"
          type="image/png"
          href="{{url("img/logo_u123_at_3_n.png")}}">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{url("img/logo_u123_at_3_n.png")}}">

    <!-- Web Fonts -->
    <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600,800&amp;subset=cyrillic,latin'>

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="{{asset("plugins/bootstrap/css/bootstrap.min.css")}}">
    <link rel="stylesheet" href="{{asset("css/shop.style.css")}}">

    <!-- CSS Header and Footer -->
    <link rel="stylesheet" href="{{asset("css/headers/header-v5.css")}}">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="{{asset("plugins/font-awesome/css/font-awesome.min.css")}}">
    <link rel="stylesheet" href="{{asset("plugins/owl-carousel/owl-carousel/owl.carousel.css")}}">


    @yield("header")
</head>
<body class="boxed-layout container">

                    <!-- Google Tag Manager -->
                    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N7BPND"
                                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
                    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                                '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                        })(window,document,'script','dataLayer','GTM-N7BPND');</script>
                    <!-- End Google Tag Manager -->

                    <script>
                        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                        ga('create', 'UA-63369856-1', 'auto');
                        ga('send', 'pageview');

                    </script>

    <div class="wrapper">
        <!--=== Header v5 ===-->
        <div class="header-v5 header-static">
        @if(Auth::check() && Auth::user()->usertype == 3)
            <!-- Topbar v3 -->
            <div class="topbar-v3">
                <div class="search-open">
                    <div class="container">


                        <div class="search-close"><i class="icon-close"></i></div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <!-- Topbar Navigation -->
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-inline right-topbar pull-right">

                                <li><a href="{{url("/admin")}}">Adminbereich</a></li>

                                    <li><i class="search fa fa-search search-button"></i>

                                    </li>
                            </ul>
                        </div>
                    </div>
                </div><!--/container-->
            </div>
            <!-- End Topbar v3 -->
        @endif

            <!-- Navbar -->
            <div class="navbar navbar-default mega-menu" role="navigation">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="{{ url('/') }}">
                            <img src="{{url("img/logo_u123_at_3_n.png")}}" id="navLogo" alt="uhren123 uhren günstig online kaufen">
                        </a>
                    </div>

                @if(isset($cart_items))

                    <!-- Shopping Cart -->
                    <div class="shop-badge badge-icons pull-right">
                        <a href="{{url("cart")}}"><i class="fa fa-shopping-cart"></i></a>
                        <span class="badge badge-sea rounded-x">{{$shopping_count}}</span>
                        <div class="badge-open">
                            <ul class="list-unstyled mCustomScrollbar" data-mcs-theme="minimal-dark">

                       ´

                            @foreach($cart_items as $item)
                                <li>
                                    <img src="{{$item->thumbnail}}_SL200_.jpg" alt="{{$item->name}}">
                                    <button type="button" asin="{{$item->asin}}" id="delete" class="close">×</button>
                                    <div class="overflow-h">
                                        <span><a href="{{url("shop/herren/v/".$item->category."/".$item->id."/".$item->seo_slug)}}">{{$item->name}}</a></span>
                                        <small>{{\Request::session()->get("shopping_cart")[$item->asin]["qty"]}} x {{number_format($item->price/100,2, ".", ".")}}</small>
                                    </div>
                                </li>
                            @endforeach
                            <div class="subtotal">
                                <div class="overflow-h margin-bottom-10">
                                    <span>Summe</span>
                                    <span class="pull-right subtotal-cost">€ {{number_format($subtotal/100,2, ".", ".")}}</span>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <a href="{{url("cart")}}" class="btn-u btn-brd btn-brd-hover btn-u-sea-shop btn-block">Einkaufswagen</a>
                                    </div>
                                    <div class="col-xs-6">
                                        <a href="{{\Session::get("amz_pruchase_url")}}" id="redirectAMZ" class="btn-u btn-u-sea-shop btn-block">Kasse</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Shopping Cart -->

                @endif
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse navbar-responsive-collapse">
                        <!-- Nav Menu -->
                        <ul class="nav navbar-nav">
                            <li><a href="{{ url('/') }}">start</a></li>
                            <!-- Damen -->
                            <li class="dropdown">
                                <a href="{{url("shop/damen/find")}}" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">
                                    Damen
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{url("shop/damen/find")}}" alt="Damenuhren">Alle</a></li>
                                    <li><a href="{{url("shop/damen/find/automatikuhren")}}" alt="Automatikuhren für Damen">Automatikuhren</a></li>
                                    <li><a href="{{url("shop/damen/find/quarzuhren")}}" alt="Quarzuhren für Damen">Quarzuhren</a></li>
                                    <li><a href="{{url("shop/damen/find/digitaluhren")}}" alt="Digitaluhren für Damen">Digitaluhren</a></li>
                                </ul>
                            </li>
                            <!-- End Damen -->
                            <!-- Herren -->
                            <li class="dropdown">
                                <a href="{{url("shop/herren/find")}}" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">
                                    Herren
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{url("shop/herren/find")}}" alt="Herrenuhren">Alle</a></li>
                                    <li><a href="{{url("shop/herren/find/automatikuhren")}}" alt="Automatikuhren für Herren">Automatikuhren</a></li>
                                    <li><a href="{{url("shop/herren/find/quarzuhren")}}" alt="Quarzuhren für Herren">Quarzuhren</a></li>
                                    <li><a href="{{url("shop/herren/find/digitaluhren")}}" alt="Digitaluhren für Herren">Digitaluhren</a></li>
                                </ul>
                            </li>
                            <!-- End Herren -->
                            <!-- Smartwatches -->
                            <li >
                                <a href="{{url("shop/unisex/find/smartwatches")}}" alt="Smartwatches" class="dropdown-toggle">
                                    Smartwatches
                                </a>
                            </li>
                            <!-- End Smartwatches -->

                            <li class="dropdown">
                                <a href="{{url("shop/haushaltsuhren/find")}}" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" data-delay="1000">
                                    Haushaltsuhren
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{url("shop/haushaltsuhren/find")}}" alt="Haushaltsuhren">Alle</a></li>
                                    <br>
                                    <li><a href="{{url("shop/haushaltsuhren/find/kuckucksuhren")}}" alt="Kuckucksuhren für den Haushalt">Kuckucksuhren</a></li>
                                    <li><a href="{{url("shop/haushaltsuhren/find/wanduhren")}}" alt="Wanduhren für zuhause">Wanduhren</a></li>
                                    <li><a href="{{url("shop/haushaltsuhren/find/pendeluhren")}}" alt="Pendeluhre ">Pendeluhren</a></li>
                                    <br>
                                    <li><a href="{{url("shop/haushaltsuhren/find/kurzzeitwecker")}}" alt="Kurzzeit Haushaltswecker">Kurzzeitwecker</a></li>
                                    <li><a href="{{url("shop/haushaltsuhren/find/haushaltswecker")}}" alt="Wecker">Haushaltswecker</a></li>
                                </ul>
                            </li>


                            <!-- Zubehör -->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" data-delay="1000">
                                    Zubehör
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{url("shop/zubeheor/find/uhrenarmbaender")}}" alt="Schöne Armbänder für Uhren">Armbänder</a></li>
                                    <li><a href="{{url("shop/reperatur/find/reperatursets")}}" alt="Uhr-Reperatur Sets">Reperatur Sets</a></li>
                                    <li><a href="{{url("shop/aufbewahrung/find/uhrenbeweger")}}" alt="Uhrenbeweger">Uhrenbeweger</a></li>
                                    <li><a href="{{url("shop/aufbewahrung/find/uhrenboxen")}}" alt="Uhrenboxen für schonende Aufbewahrung">Uhrenboxen</a></li>
                                    <li><a href="{{url("shop/batterien/find/uhrenbatterien")}}" alt="Batterien für Uhren">Batterien</a></li>
                                </ul>
                            </li>
                            <!-- End Zubehör -->


                            <li >
                                <a href="{{url("blog")}}" alt="Blog" class="dropdown-toggle">
                                    Blog
                                </a>
                            </li>

                        </ul>
                        <!-- End Nav Menu -->
                    </div>
                </div>
            </div>
            <!-- End Navbar -->
        </div>
        <!--=== End Header v5 ===-->
            @if(Session::has("notification"))
                <div class="alert alert-success fade in">
                    {{Session::get("notification")}}
                </div>
            @endif
            @if (isset($errors) && count($errors) > 0)
                @foreach ($errors as $error)
                    <div class="alert alert-danger fade in">
                        {{ $error }}
                    </div>
                @endforeach
            @endif
        @yield("content")
        <div class="footer-v4">
            <div class="footer">
                <div class="container">
                    <div class="row">
                        <!-- About -->
                        <div class="col-md-4 md-margin-bottom-40">
                            <p>Uhren123 - Eleganz die einfahch da ist.</p>
                            <br>
                            <ul class="list-unstyled address-list margin-bottom-20">
                                <li><i class="fa fa-angle-right"></i>Phone: +0043 (0)660 372 336 2</li>
                                <li><i class="fa fa-angle-right"></i>Email: info@uhren123.at</li>
                                <li>*Produkt Preise und Verfügbarkeit sind in Echtzeit und können sich ändern.
                                    Jeder Preis und jede Verfügbarkeit auf uhren123.at wird zum Zeitpunkt des Kaufes geltend gemacht
                                </li>
                            </ul>

                        </div>
                        <!-- End About -->
                        <!-- Simple List -->
                        <div class="col-md-2 col-sm-3">
                            <div class="row">
                                <div class="col-sm-12 col-xs-6">
                                    <h2 class="thumb-headline">Nach Geschlecht</h2>
                                    <ul class="list-unstyled simple-list margin-bottom-20">
                                        <li><a href="{{url("shop/damen/find")}}">Damenuhren</a></li>
                                        <li><a href="{{url("shop/herren/find")}}">Herrenuhren</a></li>
                                        <li><a href="{{url("shop/unisex/find")}}">Unisex</a></li>
                                    </ul>
                                </div>
                            </div>
                            </div>
                        <div class="col-md-2 col-sm-3">
                            <div class="row">
                                <div class="col-sm-12 col-xs-6">
                                    <h2 class="thumb-headline">Smartwatches</h2>
                                    <ul class="list-unstyled simple-list">
                                        <li><a href="{{url("/shop/unisex/find/smartwatches/apple")}}" alt="Apple Smartwatches">Apple</a></li>
                                        <li><a href="{{url("/shop/unisex/find/smartwatches/sony")}}" alt="Sony Smartwatches">Sony</a></li>
                                        <li><a href="{{url("/shop/unisex/find/smartwatches/withings")}}" alt="Withings Smartwatches">Withings</a></li>
                                        <li><a href="{{url("/shop/unisex/find/smartwatches/samsung")}}" alt="Samsung Smartwatches">Samsung</a></li>
                                    </ul>
                                </div>
                                <div class="col-sm-12 col-xs-6">
                                    <h2 class="thumb-headline">Zubehör</h2>
                                    <ul class="list-unstyled simple-list margin-bottom-20">
                                        <li><a href="{{url("shop/zubeheor/find/uhrenarmbaender")}}">Uhrenarmbänder</a></li>
                                        <li><a href="{{url("shop/aufbewahrung/find/uhrenboxen")}}">Uhrenboxen</a></li>
                                        <li><a href="{{url("shop/reperatur/find")}}">Reperatur</a></li>
                                    </ul>
                                </div>
                            </div >
                        </div>
                        <!-- End Simple List -->
                    </div><!--/end row-->
                </div><!--/end continer-->
            </div><!--/footer-->
            <div class="copyright">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <p>
                                2014 &copy; uhren123.at Alle Reachte vorbehalten
                                  <a href="{{url("datenschutzbestimmungen")}}">Datenschutzbestimmungen</a>
                                | <a href="{{url("impressum")}}">Impressum</a>
                                | <a href="{{url("sitemap")}}">Sitemap</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div><!--/copyright-->
        </div>
    </div><!--/wrapper-->

    <!-- JS Global Compulsory -->
    <script src="{{asset("plugins/jquery/jquery.min.js")}}"></script>
    <script src="{{asset("plugins/jquery/jquery-migrate.min.js")}}"></script>
    <script src="{{asset("plugins/bootstrap/js/bootstrap.min.js")}}"></script>
    <!-- JS Implementing Plugins -->


    <script src="{{asset("plugins/back-to-top.js")}}"></script>
    <script src="{{asset("plugins/owl-carousel/owl-carousel/owl.carousel.js")}}"></script>
    <script src="{{asset("plugins/scrollbar/js/jquery.mCustomScrollbar.concat.min.js")}}"></script>

    <!-- JS Page Level -->
    <script src="{{asset("js/shop.app.js")}}"></script>
    <script src="{{asset("js/plugins/owl-carousel.js")}}"></script>


    <script>
        jQuery(document).ready(function() {
            App.init();
            App.initScrollBar();
            OwlCarousel.initOwlCarousel();
        });
    </script>
    @yield("footer")
    <!--[if lt IE 9]>
    <script src="{{asset("plugins/respond.js")}}"></script>
    <script src="{{asset("plugins/html5shiv.js")}}"></script>
    <script src="{{asset("js/plugins/placeholder-IE-fixes.js")}}"></script>
    <![endif]-->


<script>
    $(document).ready(function () {
        ajaxuse = false;
        $("#delete").on("click", function () {
            if(ajaxuse == true)
            {
                return;
            }
            else
            {
                ajaxuse = true;
                $.ajax({
                    url: '{{url('cart/delete/')}}' + "/" + $(this).attr("asin"),
                    type: 'POST',
                    async: false,
                    dataType: "",
                    data: {
                        '_token': '{{csrf_token()}}'
                    },
                    success: function (result) {
                        ajaxuse = false;
                        location.reload();
                    }
                });

            }

        });
    });
</script>
</body>
</html>
















