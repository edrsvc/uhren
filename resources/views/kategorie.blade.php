@extends('layouts.app')
@section('header')
    @if(isset($search["categories"]) && count($search["categories"]) === 1)
        <title>Uhren123 - {{ucfirst($search["gender"])}} - {{ucfirst(array_values($search["categories"])[0])}}</title>
        <meta name="description" content="{{json_decode($settings[0]->category_descriptions,true)[strtolower($search["gender"])][strtolower(array_values($search["categories"])[0])]}}">

        <link rel="canonical" href="{{url("shop/".$search["gender"]."/find/".strtolower(array_values($search["categories"])[0])) }}">

    @elseif(isset($search["gender"]))
        <title>Uhren123 - {{ucfirst($search["gender"])}}</title>
        <meta name="description" content="{{json_decode($settings[0]->category_descriptions,true)[strtolower($search["gender"])]["desc"]}}">

        <link rel="canonical" href="{{url("shop/".$search["gender"]."/find") }}">

    @else
        <meta name="description" content="{{$settings[0]->description}}">
        <link rel="canonical" href="{{url() }}">
    @endif
    <meta name="keywords" content="{{$settings[0]->keywords}}">



    <link rel="stylesheet" href="{{asset("plugins/noUiSlider/jquery.nouislider.min.css")}}">
@stop
@section('content')
    <link rel="stylesheet" href="{{asset("plugins/noUiSlider/jquery.nouislider.min.css")}}">
    <!--=== Breadcrumbs v4 ===-->
    <div class="breadcrumbs-v4">
        <div class="container">
            <span class="page-name">Produkte</span>
            @if(isset($search["categories"]) && count($search["categories"]) === 1)

                <h1>{{ucfirst($search["gender"])}} - {{ucfirst(array_values($search["categories"])[0])}}</h1>
            @else
                <h1>{{ucfirst($search["gender"])}}</h1>
            @endif

            <ul class="breadcrumb-v4-in">
                <li><a href="{{url("/")}}">Startseite</a></li>
                @if(count($search["categories"]) === 1)
                    <li><a href="{{url("shop/".$search["gender"]."/find")}}">{{ucfirst($search["gender"]) }}</a></li>

                    <li class="active">{{ucfirst(array_values($search["categories"])[0])}}</li>
                </ul>

                  <p id="catPText"> {{
            json_decode($settings[0]->category_descriptions,true)
            [strtolower($search["gender"])]
            [strtolower(array_values($search["categories"])[0])]
                 }}</p>
                @else
                    <li class="active">{{ucfirst($search["gender"])}}</li>
                <p id="catPText"> {{json_decode($settings[0]->category_descriptions,true)[strtolower($search["gender"])]["desc"]}}</p>

                @endif


        </div>
        <!--/end container-->
    </div>
    <!--=== End Breadcrumbs v4 ===-->
    <div class="content container">
        @if(isset($search["brands"]))
            <div class="row">
                <div class="col-md-3 filter-by-block md-margin-bottom-60">
                    <h3>Filtern</h3>
                    {{ Form::open(array('url' => [Request::fullUrl() ,  $search["gender"]], "class" => "sky-form", 'method' => 'GET')) }}
                    @if(isset($search["brands"]))
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                            Marken
                                            <i class="fa fa-angle-down"></i>
                                        </a>
                                    </h2>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="panel-body" style="max-height: 200px;overflow-y: scroll;">
                                        <ul class="list-unstyled checkbox-list" >
                                            @foreach($search["brands"] as $brand)
                                                <li>
                                                    <label class="checkbox" >
                                                        <input  {!! null !== old('brands') ?  in_array($brand, old('brands') ) ? 'checked' : null : null !!} type="checkbox"  name="brands[]" value="{{$brand}}"  />
                                                        <i></i>
                                                        {{$brand}}
                                                        <small><a href="#"></a></small>
                                                    </label>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                <!--/end panel group-->
                    @if(count($search["categories"]) > 1)
                        <div class="panel-group" id="accordion-v2">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion-v2" href="#collapseTwo">
                                            Kategorien
                                            <i class="fa fa-angle-down"></i>
                                        </a>
                                    </h2>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse in">
                                    <div class="panel-body" >
                                        <ul class="list-unstyled checkbox-list">
                                            @foreach($search["categories"] as $category)
                                                <li>
                                                    <label class="checkbox">
                                                        <input {!! null !== old('categories') ?  in_array($category, old('categories') ) ? 'checked' : null : null !!} type="checkbox" name="categories[]" value="{{$category}}"  />
                                                        <i></i>
                                                        {{$category}}
                                                        <small><a href="#"></a></small>
                                                    </label>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                <!--/end panel group-->
                    @if(isset($search["maxprice"]) && $search["maxprice"] != 0)
                        <div class="panel-group" id="accordion-v4">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion-v4" href="#collapseFour">
                                            Preis
                                            <i class="fa fa-angle-down" id="range"></i>
                                        </a>
                                    </h2>
                                </div>
                                <div id="collapseFour" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="slider-snap" id="slider-snap"></div>
                                        <p class="slider-snap-text">
                                            <input type="number" step="0.01" class="slider-snap-value-lower" name="price[lower]" id="slider-snap-value-lower">
                                            <input type="number" step="0.01" class="slider-snap-value-lower" name="price[upper]" id="slider-snap-value-upper">
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                <!--/end panel group-->
                    @if(isset($search["colors"]) && count($search["colors"]) >= 1)
                        <div class="panel-group" id="accordion-v5">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion-v5" href="#collapseFive">
                                            Farbe
                                            <i class="fa fa-angle-down"></i>
                                        </a>
                                    </h2>
                                </div>
                                <div id="collapseFive" class="panel-collapse collapse in">
                                    <div class="panel-body" style="max-height: 200px;overflow-y: scroll;">
                                            <ul class="list-unstyled checkbox-list">
                                                @foreach($search["colors"] as $color)
                                                    <li>
                                                        <label class="checkbox">
                                                            <input {!! null !== old('colors') ?  in_array($color, old('colors') ) ? 'checked' : null : null !!} type="checkbox" name="colors[]" value="{{$color}}"  />
                                                            <i></i>
                                                            {{$color}}
                                                            <small><a href="#"></a></small>
                                                        </label>
                                                    </li>
                                                @endforeach
                                            </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                <!--/end panel group-->
                    <button type="submit" class="btn-u">Anwenden</button>
                    {{ Form::close() }}
                </div>
                <div class="col-md-9">
                    <div class="row margin-bottom-5">
                        <div class="col-sm-4 result-category">
                            <h2>
                                @if(count($search["categories"]) === 1)
                                    {{array_values($search["categories"])[0]}}
                                @else
                                    {{$search["gender"]}}
                                @endif
                            </h2>
                        </div>
                        <div class="col-sm-8">
                            <ul class="list-inline clear-both">
                                <li class="grid-list-icons">
                                    <i class="showkind fa fa-th-list" set="list"></i>
                                    <i class="showkind fa fa-th" set="tile"></i>
                                </li>
                                <li class="sort-list-btn">
                                    <label for="show">Sortierung &nbsp;&nbsp;</label>
                                    <select id="sortby">
                                        <option value="price desc">Preis absteigend</option>
                                        <option value="price asc">Preis aufsteigend</option>
                                        <option value="popularity desc">Beliebtheit absteigend</option>
                                        <option value="popularity asc">Beliebtheit absteigend</option>
                                    </select>
                                </li>
                                <li class="sort-list-btn">
                                    <label for="show">Anzeige &nbsp;&nbsp;</label>
                                    <select id="show">
                                        <option value="6">6</option>
                                        <option value="12">12</option>
                                        <option value="18">18</option>
                                        <option value="all">Alle</option>
                                    </select>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--/end result category-->
                    <div class="filter-results">
                        <div class="row illustration-v2 margin-bottom-30">
                            @if($products === FALSE)
                                <!--NO PRODUCTS FOUND-->
                                <div class="col-md-6">
                                    <div class="alert alert-info fade in">
                                        <h4>Info! Leider nichts gefunden</h4>
                                        <p>Ich habe gesucht, gesucht und gesucht. Und trotzdem konnte Ich nichts passendes finden.</p>
                                        <p>Verändern Sie die Sucheinstellungen.</p>
                                    </div>
                                </div>
                                <!-- /NO PRODUCTS FOUND-->
                            @else
                                @if(!Session::has("showkind") || Session::get("showkind") == "tile")
                                    @foreach($products as $product)
                                        <div class="col-md-4" id="catProduct" style="overflow: hidden;">

                                            <div class="product-img product-img-brd" style="height: 290px;">

                                                <a href="{{url("shop/".$product->gender."/v/".$product->category."/".$product->id."/".$product->seo_slug)}}">

                                                    <img class="img-responsive" id="catImg" src="{{$product->thumbnail}}_SL290_.jpg" alt="{{substr ($product->name,0,66) }}">

                                                </a>



                                            </div>
                                            <div class="product-description product-description-brd margin-bottom-30">
                                                <div class="overflow-h margin-bottom-5">
                                                    <div class="pull-left">
                                                        <h4 class="title-price">
                                                            <a href="{{url("shop/".$product->gender."/v/".$product->category."/".$product->id."/".$product->seo_slug)}}"><small>{{substr ($product->name,0,66) }}  </small></a>
                                                        </h4>
                                                        <span class="gender text-uppercase">
                                                            <span class="gender">{{$product->gender}}</span>
                                                        </span>
                                                        <span class="gender">{{$product->category}}</span>
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="title-price">€ {{number_format($product->price/100,2, ",", ".")}}*</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @elseif(Session::get("showkind") == "list")
                                    @foreach($products as $product)
                                            <div class="list-product-description product-description-brd margin-bottom-30">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <a href="{{url("shop/".$product->gender."/v/".$product->category."/".$product->id."/".$product->seo_slug)}}">
                                                            <img class="img-responsive " id=""  src="{{$product->thumbnail}}_SL290_.jpg" alt="{{substr ($product->name,0,66) }}">
                                                        </a>
                                                    </div>
                                                    <div class="col-sm-8 product-description">
                                                        <div class="overflow-h margin-bottom-5">
                                                            <ul class="list-inline overflow-h">
                                                                <li><h4 class="title-price"><a href="{{url("shop/".$product->gender."/v/".$product->category."/".$product->id."/".$product->seo_slug)}}">{{substr ($product->name,0,66) }}  </a></h4></li>
                                                                <li><span class="gender text-uppercase">{{$product->gender}}</span></li>

                                                            </ul>
                                                            <div class="margin-bottom-10">
                                                                <span class="title-price margin-right-10">€ {{number_format($product->price/100,2, ",", ".")}}</span>
                                                            </div>

                                                            <button type="button"  id="cartadd" class="btn-u btn-u-sea-shop" asin="{{$product->asin}}" qty="1">Add to Cart</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    @endforeach
                                @endif
                                <!--PAGINATION-->
                                    <div class="text-center">
                                        {!! $products->appends(Request::except("page","_token"))->render() !!}
                                    </div>
                            @endif
                        </div>
                    </div>
                </div>
                @if(isset($recommendations) && !empty($recommendations))
                    <div class="container">
                        <div class="heading heading-v1 margin-bottom-20">
                            <h2>Empfehlungen</h2>
                            <p>
                                Wir glauben, dass Ihnen diese Produkte gefallen!
                            </p>
                        </div>
                        <div class="illustration-v2 margin-bottom-60">
                            <div class="customNavigation margin-bottom-25">
                                <a class="owl-btn prev rounded-x"><i class="fa fa-angle-left"></i></a>
                                <a class="owl-btn next rounded-x"><i class="fa fa-angle-right"></i></a>
                            </div>
                            <ul class="list-inline owl-slider-v4">
                                @foreach($recommendations as $recommend)
                                    <li class="item">
                                        <a href="{{url("shop/".$recommend->gender."/v/".$recommend->category."/".$recommend->id."/".$recommend->seo_slug)}}">
                                            <img class="img-responsive" id="recommendationsPrt" src="{{$recommend->thumbnail}}_SL180_.jpg" alt="{{$recommend->name}}">
                                        </a>
                                        <div class="product-description-v2">
                                            <div class="margin-bottom-5">
                                                <h4 class="title-price"><a href="{{url("shop/".$recommend->gender."/v/".$recommend->category."/".$recommend->id."/".$recommend->seo_slug)}}">{{$recommend->name}}</a></h4>
                                                <span class="title-price">€ {{number_format($recommend->price/100,2, ",", ".")}}*</span>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif
            <!--/end pagination-->
            </div>
            @else
            <h1>404</h1>
            <p>Seite nicht verfügbar
            <ul>
                <li><a href="{{url("/")}}">Startseite</a></li>
                <li><a href="{{url("/contact")}}">Kontakt</a> </li>
                <li><a href="{{ URL::previous() }}">Zurück</a> </li>
            </ul>
            </p>
            @if(isset($recommendations) && !empty($recommendations))
                <div class="container">
                    <div class="heading heading-v1 margin-bottom-20">
                        <h2>Empfehlungen</h2>
                        <p>
                            Wir glauben, dass Ihnen diese Produkte gefallen!
                        </p>
                    </div>
                    <div class="illustration-v2 margin-bottom-60">
                        <div class="customNavigation margin-bottom-25">
                            <a class="owl-btn prev rounded-x"><i class="fa fa-angle-left"></i></a>
                            <a class="owl-btn next rounded-x"><i class="fa fa-angle-right"></i></a>
                        </div>
                        <ul class="list-inline owl-slider-v4">
                            @foreach($recommendations as $recommend)
                                <li class="item">
                                    <a href="{{url("shop/".$recommend->gender."/v/".$recommend->category."/".$recommend->id."/".$recommend->seo_slug)}}">
                                        <img class="img-responsive"  id="recommendationsPrt"  src="{{$recommend->thumbnail}}_SL180_.jpg" alt="">
                                    </a>
                                    <div class="product-description-v2">
                                        <div class="margin-bottom-5">
                                            <h4 class="title-price"><a href="{{url("shop/".$recommend->gender."/v/".$recommend->category."/".$recommend->id."/".$recommend->seo_slug)}}">{{$recommend->name}}</a></h4>
                                            <span class="title-price">€ {{number_format($recommend->price/100,2, ",", ".")}}*</span>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
        @endif
    </div>
@endsection
@section("footer")
    <script src="https://raw.githubusercontent.com/leongersen/noUiSlider/master/distribute/nouislider.min.js"></script>
    <script>
        // jQuery
        $('document').ready(function(){

            $(".showkind").on("click", function () {
                if(ajaxuse == true)
                {
                    return;
                }
                else
                {
                    ajaxuse = true;
                    $.ajax({
                        url: '{{url('visitor/edit/showkind/')}}' + "/" + $(this).attr("set"),
                        type: 'POST',
                        async: false,
                        dataType: "",
                        data: {
                            '_token': '{{csrf_token()}}'
                        },
                        success: function (result) {
                            location.reload();
                        }
                    });
                    ajaxuse = false;
                }
            });

            $("#cartadd").on("click", function () {
                if(ajaxuse == true)
                {
                    return;
                }
                else
                {
                    ajaxuse = true;
                    $.ajax({
                        url: '{{url('cart/add/')}}' + "/" + $(this).attr("asin") + "/" + $(this).attr("qty"),
                        type: 'POST',
                        async: false,
                        dataType: "",
                        data: {
                            '_token': '{{csrf_token()}}'
                        },
                        success: function (result) {
                            location.reload();
                        }
                    });
                    ajaxuse = false;
                }
            });
// Limit products/per page
            $("#show").on("change", function () {
                if(ajaxuse == true)
                {
                    return;
                }
                else
                {
                    ajaxuse = true;
                    $.ajax({
                        url: '{{url('visitor/edit/show/')}}' + "/" + $(this).val(),
                        type: 'POST',
                        async: false,
                        dataType: "",
                        data: {
                            '_token': '{{csrf_token()}}'
                        },
                        success: function (result) {
                            location.reload();
                        }
                    });
                    return html;
                }
            });
// Sorting
            $('#sortby').on("change", function () {
                if(ajaxuse == true)
                {
                    return;
                }
                else
                {
                    ajaxuse = true;
                    $.ajax({
                        url: '{{url('visitor/edit/sort/')}}' + "/" + $(this).val(),
                        type: 'POST',
                        async: false,
                        dataType: "",
                        data: {
                            '_token': '{{csrf_token()}}'
                        },
                        success: function (result) {
                            location.reload();
                        }
                    });
                    return html;
                }
            });

            ajaxuse = false;
            var range = document.getElementById('slider-snap');
            var rangeValues = [
                document.getElementById('slider-snap-value-lower'),
                document.getElementById('slider-snap-value-upper')
            ];

            noUiSlider.create(range, {
                start: [
                    {!! null !== old('price.lower') ?  old('price.lower') : (($search["minprice"]) / 100)  !!},
                    {!! null !== old('price.upper') ?  old('price.upper') : ( ($search["maxprice"]) / 100) !!}
                ],
                snap: false,
                connect: true,
                range: {
                    'min': {{($search["minprice"]) / 100}},
                    'max': {{($search["maxprice"]) / 100}}
                }
            });
            range.noUiSlider.on('update', function( values, handle ) {
                rangeValues[handle].value = values[handle];
            });
        });
    </script>
@stop
