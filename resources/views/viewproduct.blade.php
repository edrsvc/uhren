@extends('layouts.app')
@section("header")


    <title>{{($meta["title"])}}</title>
    <meta name="description" content="{{($meta["description"])}}">
    <meta name="keywords" content="{{($meta["keywords"])}}">


    <link rel="stylesheet" href="{{asset("plugins/master-slider/masterslider/style/masterslider.css")}}">
    <link rel='stylesheet' href="{{asset("plugins/master-slider/masterslider/skins/light-6/style.css")}}">
@stop
@section('content')
    <div class="shop-product">
        <div class="container">
            <ul class="breadcrumb-v5">
                <li><a href="{{url("/")}}"><i class="fa fa-home"></i></a></li>
                <li>
                    <a href="{{url("shop/".$productArray["gender"]."/find" ) }}">
                        {{ucfirst($productArray["gender"]) }}
                    </a>
                </li>
                <li class="active">
                    <a href="{{url("shop/".$productArray["gender"]."/find/".$productArray["category"]  ) }}">
                    {{$productArray["category"] }}</li>
                </a>
            </ul>
        </div>
        <!-- End Breadcrumbs v5 -->
        <div class="container">
            <div class="row">
                <div class="col-md-6 md-margin-bottom-50">
                    <div class="ms-showcase2-template">
                        <!-- Master Slider -->
                        <div class="master-slider ms-skin-light-6" id="masterslider">
                            <div class="ms-slide">
                                <img class="ms-brd" id="masterSliderImageAdjustments" src="{{asset('img/blank.gif')}}" data-src="{{$productArray["thumbnail"]}}_SL800_.jpg" alt="{{($productArray["name"])}}">
                                <img class="ms-thumb" id="masterSliderImageAdjustments" src="{{$productArray["thumbnail"]}}_SL100_.jpg" width="inherit" alt="thumb">
                            </div>
                            @foreach($productArray["imgurls"] as $img)
                                <div class="ms-slide">
                                    <img class="ms-brd" id="masterSliderImageAdjustments" src="{{asset('img/blank.gif')}}" data-src="{{$img}}_SL800_.jpg" alt="{{($productArray["name"])}}">
                                    <img class="ms-thumb" id="masterSliderImageAdjustments" src="{{$img}}_SL100_.jpg" width="inherit" alt="thumb">
                                </div>
                            @endforeach
                        </div>

                        <!-- End Master Slider -->
                    </div>
                </div>
                <div class="col-md-6">
                    <div itemscope itemtype="http://schema.org/Product" >
                        <div class="shop-product-heading">
                            <h1 itemprop="name">{{($productArray["name"])}}</h1><br>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_sharing_toolbox"></div>
                        </div><!--/end shop product social-->
                        <div itemprop="brand" itemscope itemtype="http://schema.org/Organization">
                            <span itemprop="name">{{$productArray["brand"]}}</span>
                        </div>

                        <p itemprop="description">
                            {!!$productArray["editorreview"]!!}
                            <br>
                            @if(isset($productArray["features"]))
                            <h3 class="shop-product-title">Features</h3>
                            <ul>
                                @foreach($productArray["features"] as $feature)
                                    <li>
                                        {{$feature}}
                                    </li>
                                @endforeach
                            </ul>
                            @endif
                        </p>

                        <ul class="list-inline shop-product-prices margin-bottom-30" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                            <li class="shop-bg-green">
                                <span itemprop="priceCurrency">EURO</span> <span itemprop="price">{{number_format( ($productArray["price"]) / 100,2, ".", ".")}}</span>*
                            </li>
                            <li>
                                <link itemprop="itemCondition" href="http://schema.org/NewCondition" /> Neu
                            </li>
                        </ul><!--/end shop product prices-->

                        <h3 class="shop-product-title">Menge</h3>
                        <div class="margin-bottom-40">
                            <form name="f1" class="product-quantity sm-margin-bottom-20">
                                <button type='button' class="quantity-button" name='subtract' onclick='javascript: subtractQty();' value='-'>-</button>
                                <input type='text' class="quantity-field" name='qty' value="1" id='qty'/>
                                <button type='button' class="quantity-button" name='add' onclick='javascript: document.getElementById("qty").value++;' value='+'>+</button>
                            </form>
                            <button type="button" id="addcart" class="btn-u btn-u-sea-shop btn-u-lg">In den Einkaufswagen</button>
                        </div><!--/end product quantity-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(isset($recommendations) && !empty($recommendations))
        <div class="container">
            <div class="heading heading-v1 margin-bottom-20">
                <h2>Empfehlungen</h2>
                <p>
                    Wir glauben, dass Ihnen diese Produkte gefallen!
                </p>
            </div>

            <div class="illustration-v2 margin-bottom-60">
                <div class="customNavigation margin-bottom-25">
                    <a class="owl-btn prev rounded-x"><i class="fa fa-angle-left"></i></a>
                    <a class="owl-btn next rounded-x"><i class="fa fa-angle-right"></i></a>
                </div>

                <ul class="list-inline owl-slider-v4">
                    @foreach($recommendations as $recommend)
                        <li class="item">
                            <a href="{{url("shop/".$recommend->gender."/v/".$recommend->category."/".$recommend->id."/".$recommend->seo_slug)}}">
                                <img class="img-responsive"   id="recommendationsPrt" src="{{$recommend->thumbnail}}_SL180_.jpg" alt="">
                            </a>
                            <div class="product-description-v2">
                                <div class="margin-bottom-5">
                                    <h4 class="title-price"><a href="{{url("shop/".$recommend->gender."/v/".$recommend->category."/".$recommend->id."/".$recommend->seo_slug)}}">{{$recommend->name}}</a></h4>
                                    <span class="title-price">€ {{number_format($recommend->price/100,2, ",", ".")}}*</span>
                                </div>

                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif
@endsection

@section("footer")

    <script src="{{asset("js/plugins/master-slider.js")}}"></script>

    <script src="{{asset("plugins/master-slider/masterslider/masterslider.min.js")}}"></script>
    <script src="{{asset("plugins/master-slider/masterslider/jquery.easing.min.js")}}"></script>

    <script>
        jQuery(document).ready(function() {
            ajaxuse = false;
            cardadded = false;
            MasterSliderShowcase2.initMasterSliderShowcase2();

            $("#addcart").on("click", function () {


                if(ajaxuse == true)
                {
                    return;
                }
                else
                {
                    ajaxuse = true;
                    $.ajax({
                        url: '{{url('cart/add/')}}' + "/{{$productArray['asin']}}/" + $("#qty").val(),
                        type: 'POST',
                        async: false,
                        dataType: "",
                        data: {
                            '_token': '{{csrf_token()}}'
                        },
                        success: function (result) {
                            cardadded = true;
                            location.reload();
                        }
                    });
                    ajaxuse = false;
                }

            })

        });

    </script>

    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57a76d5927f68859"></script>

@stop