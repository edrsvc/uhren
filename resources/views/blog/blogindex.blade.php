@extends('layouts.app')
@section("header")
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57a76d5927f68859"></script>

    <title>Uhren123 Blog - Alles rund um Uhren und mehr</title>
    <meta name="description" content="Sie lieben Uhren? Wir tun dies auch. Erfahren Sie hier aktuelle Neuigkeiten, tipps und Produktempfehlungen.">
    <meta name="keywords" content="produktempfehlungen, blog, uhren, uhrenblog, news">

@stop
@section('content')
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left">Uhren123 Blog</h1>
            <ul class="pull-right breadcrumb">
                <li><a href="{{url("blog")}}">Blog</a></li>
            </ul>
        </div>
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->
    <!--=== Content Part ===-->
    <div class="container content">
        <div class="row blog-page">
            <!-- Left Sidebar -->
            <div class="col-md-9 md-margin-bottom-40">


                @foreach($articles as $article)
                    <!--Blog Post-->
                        <div class="row blog blog-medium margin-bottom-40">
                            <div class="col-md-5">
                                <img class="img-responsive" src="{{url("uploads/".$article->article_image)}}" alt="">
                            </div>
                            <div class="col-md-7">
                                <h2><a href="{{url("/blog/view/".$article->id."/".$article->seo_slug)}}">{{$article->title}}</a></h2>
                                <ul class="list-unstyled list-inline blog-info">
                                    <li><i class="fa fa-calendar"></i> {{$article->created_at->diffForHumans()}}</li>
                                </ul>
                                <p>{{$article->description}}</p>
                                <p><a class="btn-u btn-u-sm" href="{{url("/blog/view/".$article->id."/".$article->seo_slug)}}">Weiterlesen <i class="fa fa-angle-double-right margin-left-5"></i></a></p>
                            </div>
                        </div>
                        <!--End Blog Post-->

                @endforeach

                <hr class="margin-bottom-40">




                <!--Pagination-->
                <div class="text-center">
                    {!! $articles->render() !!}
                </div>
                <!--End Pagination-->
            </div>
            <!-- End Left Sidebar -->

            <!-- Right Sidebar -->
            <div class="col-md-3">
                <!-- Social Icons -->
                <div class="magazine-sb-social margin-bottom-30">
                    <div class="headline headline-md"><h2>Folgen</h2></div>
                    <!-- Go to www.addthis.com/dashboard to customize your tools -->
                    <div class="addthis_inline_follow_toolbox"></div>
                    <div class="clearfix"></div>
                </div>
                <!-- End Social Icons -->


                <!-- End Posts -->

                <!-- Tabs Widget -->
                <div class="tab-v2 margin-bottom-40">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#home-1">About Us</a></li>

                    </ul>
                    <div class="tab-content">
                        <div id="home-1" class="tab-pane active">
                            <p>
                                Uhren123 ist spezialisert auf Uhren, wir bieten günstige Preise mit bester Qualität bei schneller Lieferung
                            </p>
                        </div>

                    </div>
                </div>
                <!-- End Tabs Widget -->


            </div>
            <!-- End Right Sidebar -->
        </div><!--/row-->
    </div><!--/container-->
    <!--=== End Content Part ===-->

@endsection

@section("footer")

@stop