
@extends('layouts.app')
@section("header")
    <title>Uhren123 {{$article->title}}</title>
    <meta itemprop='keywords' name="description" content="{{$article->description}}">
    <meta name="keywords" content="uhren blog, blog, uhren,artikel">

    <script type="application/ld+json">
    {
        "@context": "http://schema.org",
         "@type": "Article",
         "mainEntityOfPage": {
            "@type": "WebPage",
            "@id": "{{Request::url()}}"
        },
         "headline": "{{$article->title}}",
         "image": {
            "@type": "ImageObject",
            "url":"{{asset("uploads/".$article->article_image)}}",
            "height": {{ $imgsize[1]}},
            "width": {{ $imgsize[0]}}
          },
         "author": "UHren123",
         "editor": "Simon Daum",
         "genre": "Uhren {{$article->category}}",
         "keywords": "{{$article->keywords}}",
         "publisher": {
            "@type": "Organization",
            "name":"uhren123",
            "logo": {
                "@type": "ImageObject",
                "url": "https://uhren123.at/img/logo.png",
                "width": 600,
                "height": 60
            }
        },
         "url": "http://uhren.at",
         "datePublished": "{{$article->created_at}}",
         "dateCreated": "{{$article->created_at}}",
         "dateModified": "{{$article->updated_at}}",
         "description": "{{$article->description}}",
         "articleBody": "{{$article->text}}"
     }
    </script>

@stop
@section('content')
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <ul class="pull-right breadcrumb">
                <li><a href="{{url("blog")}}">Blog</a></li>
                <li active>{{$article->title}}</li>
            </ul>
        </div>
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <!--=== Content Part ===-->
    <div class="container content">
        <div class="row blog-page blog-item">

            <!-- Left Sidebar -->
            <div class="col-md-9 md-margin-bottom-60"  >
                <h1  ><a href="#">{{$article->title}}</a></h1>
                <div class="blog-post-tags">
                    <ul class="list-unstyled list-inline blog-info">
                        <li><i class="fa fa-calendar"></i> {{$article->created_at->diffforhumans()}}</li>
                        <li><i class="fa fa-pencil"></i> Uhren123 Team</li>
                    </ul>
                </div>

                <!--Blog Post-->
                <div class="blog margin-bottom-40">
                    {!!$article->text !!}
                    <hr>
                    <!-- Go to www.addthis.com/dashboard to customize your tools -->
                        <div class="addthis_native_toolbox"></div>
                    <!-- Go to www.addthis.com/dashboard to customize your tools -->
                        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57a76d5927f68859"></script>

                </div>
                <!--End Blog Post-->


                <div id="disqus_thread"></div>
                <script>

                    /**
                     *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                     *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables */
                    /*
                     var disqus_config = function () {
                     this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
                     this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                     };
                     */
                    (function() { // DON'T EDIT BELOW THIS LINE
                        var d = document, s = d.createElement('script');
                        s.src = '//uhren123.disqus.com/embed.js';
                        s.setAttribute('data-timestamp', +new Date());
                        (d.head || d.body).appendChild(s);
                    })();
                </script>
                <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
            </div>
            <!-- End Left Sidebar -->
        </div><!--/row-->
    </div><!--/container-->
    <!--=== End Content Part ===-->

@endsection

@section("footer")

@stop









