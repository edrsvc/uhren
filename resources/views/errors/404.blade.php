@extends('layouts.app')
@section("header")
    <link rel='stylesheet' href="{{asset("css/pages/page_404_error6.css")}}">
@stop

@section("content")
    <!--=== Error V7 ===-->
    <div class="error-bg">
        <div class="container">
            <!--Error Block-->
            <div class="row">




                <div class="col-md-8 col-md-offset-2" >
                    <script>
                        (function() {
                            var cx = '009986825427208110488:cap_csqjrmu';
                            var gcse = document.createElement('script');
                            gcse.type = 'text/javascript';
                            gcse.async = true;
                            gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
                            var s = document.getElementsByTagName('script')[0];
                            s.parentNode.insertBefore(gcse, s);
                        })();
                    </script>
                    <gcse:search></gcse:search>
                    <div class="error-v7">
                        <h1>Nichts gefunden!</h1>
                        <span class="sorry">Der Webserver (auf dem die Website läuft) denkt, dass der vom Client (z.B. Ihr Webbrowser) gesendete HTTP-Datenstrom korrekt war, aber einfach den Zugriff auf die durch die URL spezifizierte Ressource nicht liefern kann. Dies ist gleichbedeutend mit der Reaktion 'Zurück an den Absender - Adresse unbekannt' bei konventionellen Postdiensten.</span>
                        <strong class="h1">404</strong>
                    </div>
                </div>
            </div>

        </div><!--/container-->

    </div><!--/error-bg-->
    <!--products-->

    <div class="illustration-v2 ">
        <div class="heading heading-v1 margin-bottom-40">
            <h2>Unsere Empfehlungen</h2>
        </div>
        <div class="customNavigation margin-bottom-25">
            <a class="owl-btn prev rounded-x"><i class="fa fa-angle-left"></i></a>
            <a class="owl-btn next rounded-x"><i class="fa fa-angle-right"></i></a>
        </div>
        <ul class="list-inline owl-slider">
            @foreach($products as $latest)
                <li class="item">
                    <div class="product-img">
                        <a href="shop-ui-inner.html"><img class="full-width img-responsive" id="catImgB" src="{{$latest->thumbnail}}_SL250_.jpg" alt="{{$latest->name}}"></a>
                    </div>
                    <div class="product-description product-description-brd">
                        <div class="overflow-h margin-bottom-5">
                            <div class="pull-left">
                                <h4 class="title-price"><a href="{{url("shop/".$latest->gender."/v/".$latest->category."/".$latest->id."/".$latest->seo_slug)}}">{{$latest->name}}</a></h4>
                                <span class="gender text-uppercase">
                                    @if($latest->gender == 0)
                                        <span class="gender">Damen</span>
                                    @elseif($latest->gender == 1)
                                        <span class="gender">Herren</span>
                                    @elseif($latest->gender == 2)
                                        <span class="gender">Unisex</span>
                                    @endif
                                </span>
                                <span class="gender">{{$latest->category}}</span>
                            </div>
                            <div class="product-price">
                                <span class="title-price">€ {{number_format($latest->price/100,2, ",", ".")}}*</span>
                            </div>
                        </div>

                    </div>
                </li>
            @endforeach
        </ul>

    </div>


    <!--navigation-->
    <div class="container errNav">
        <h2>
            Weiter Navigationsmöglichkeiten
        </h2>
        <aside>
            Wenn Sie nicht fündig werden könnten Sie auch hier zum Ziel gelangen
        </aside>
        <h3>Kategorien</h3>
        <br>
        <div class="col-md-3">
            <ul>
                <li>Damen:
                    <ul>
                        <li><a href="{{url("shop/damen/find")}}">Alle</a></li>
                        <li><a href="{{url("shop/damen/find/automatikuhren")}}">Automatikuhren</a></li>
                        <li><a href="{{url("shop/damen/find/quarzuhren")}}">Quarzuhren</a></li>
                        <li><a href="{{url("shop/damen/find/digitaluhren")}}">Digitaluhren</a></li>
                    </ul>
                </li>
            </ul>
            <ul>
                <li>Herren:
                    <ul>
                        <li><a href="{{url("shop/herren/find")}}">Alle</a></li>
                        <li><a href="{{url("shop/herren/find/automatikuhren")}}">Automatikuhren</a></li>
                        <li><a href="{{url("shop/herren/find/quarzuhren")}}">Quarzuhren</a></li>
                        <li><a href="{{url("shop/herren/find/digitaluhren")}}">Digitaluhren</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="col-md-3">
            <ul>
                <li>Haushaltsuhren:
                    <ul>
                        <li><a href="{{url("shop/haushaltsuhren/find")}}">Alle</a></li>
                        <li><a href="{{url("shop/haushaltsuhren/find/kuckucksuhren")}}">Kuckucksuhren</a></li>
                        <li><a href="{{url("shop/haushaltsuhren/find/wanduhren")}}">Wanduhren</a></li>
                        <li><a href="{{url("shop/haushaltsuhren/find/pendeluhren")}}">Pendeluhren</a></li>
                        <li><a href="{{url("shop/haushaltsuhren/find/kurzzeitwecker")}}">Kurzzeitwecker</a></li>
                        <li><a href="{{url("shop/haushaltsuhren/find/haushaltswecker")}}">Haushaltswecker</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="col-md-3">
            <ul>
                <li>Zubehör:
                    <ul>
                        <li><a href="{{url("shop/zubeheor/find/uhrenarmbaender")}}">Armbänder</a></li>
                        <li><a href="{{url("shop/reperatur/find/reperatursets")}}">Reperatur-Sets</a></li>
                        <li><a href="{{url("shop/aufbewahrung/find/uhrenbeweger")}}">Uhrenbeweger</a></li>
                        <li><a href="{{url("shop/aufbewahrung/find/uhrenboxen")}}">Uhrenboxen</a></li>
                        <li><a href="{{url("shop/batterien/find/uhrenbatterien")}}">Batterien</a></li>
                    </ul>
                </li>
            </ul>
        </div>


    </div>

     @stop