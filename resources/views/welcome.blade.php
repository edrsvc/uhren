@extends('layouts.app')
@section("header")

    @if(isset($search["categories"]) && count($search["categories"]) === 1)
        <title>Uhren123 - {{ucfirst($search["gender"])}} - {{ucfirst(array_values($search["categories"])[0])}}</title>
        <meta name="description" content="{{json_decode($settings[0]->category_descriptions,true)[strtolower($search["gender"])][strtolower(array_values($search["categories"])[0])]}}">
    @elseif(isset($search["gender"]))
        <title>Uhren123 - {{ucfirst($search["gender"])}}</title>
        <meta name="description" content="{{json_decode($settings[0]->category_descriptions,true)[strtolower($search["gender"])]["desc"]}}">
    @else
        <meta name="description" content="{{$settings[0]->description}}">
    @endif
    <meta name="keywords" content="{{$settings[0]->keywords}}">
    <title>{{($settings[0]->title)}}</title>



    <link rel="stylesheet" href="{{asset("plugins/revolution-slider/rs-plugin/css/settings.css")}}">
@stop

@section('content')
<div class="tp-banner-container">
    <div class="tp-banner">
        <ul>
            <!-- SLIDE -->
            <li class="revolution-mch-1" data-transition="fade" data-slotamount="5" data-masterspeed="1000" data-title="Damenuhren">
                <!-- MAIN IMAGE -->
                <img src="{{asset("img/schoene-damenuhren-guenstig.jpg")}}"  alt="darkblurbg"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">

                <div class="tp-caption revolution-ch1 sft start"
                     data-x="center"
                     data-hoffset="0"
                     data-y="100"
                     data-speed="1500"
                     data-start="500"
                     data-easing="Back.easeInOut"
                     data-endeasing="Power1.easeIn"
                     data-endspeed="300">
                    Tolle <br>
                    <strong>Damenuhren</strong><br>

                </div>

                <!-- LAYER -->
                <div class="tp-caption sft"
                     data-x="center"
                     data-hoffset="0"
                     data-y="380"
                     data-speed="1600"
                     data-start="1800"
                     data-easing="Power4.easeOut"
                     data-endspeed="300"
                     data-endeasing="Power1.easeIn"
                     data-captionhidden="off"
                     id="adjZIndex">
                    <a href="{{url("shop/damen/find")}}" class="btn-u btn-brd btn-brd-hover btn-u-light">Jetzt Shoppen</a>
                </div>
            </li>
            <!-- END SLIDE -->

            <!-- SLIDE -->
            <li class="revolution-mch-1" data-transition="fade" data-slotamount="5" data-masterspeed="1000" data-title="Herrenuhren">
                <!-- MAIN IMAGE -->
                <img src="{{asset("img/herrenuhren-guenstig.jpg")}}"  alt="darkblurbg"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">

                <div class="tp-caption revolution-ch1 sft start"
                     data-x="center"
                     data-hoffset="0"
                     data-y="100"
                     data-speed="1500"
                     data-start="500"
                     data-easing="Back.easeInOut"
                     data-endeasing="Power1.easeIn"
                     data-endspeed="300">
                    Maskulin <br>
                </div>

                <!-- LAYER -->
                <div class="tp-caption sft"
                     data-x="center"
                     data-hoffset="0"
                     data-y="380"
                     data-speed="1600"
                     data-start="1800"
                     data-easing="Power4.easeOut"
                     data-endspeed="300"
                     data-endeasing="Power1.easeIn"
                     data-captionhidden="off"
                     id="adjZIndex">
                    <a href="{{url("shop/herren/find")}}" class="btn-u btn-brd btn-brd-hover btn-u-light">Jetzt Shoppen</a>
                </div>
            </li>
            <!-- END SLIDE -->


        </ul>
        <div class="tp-bannertimer tp-bottom"></div>
    </div>
</div>
<div class="container content-md">
    <div class="heading heading-v1 margin-bottom-20">
        <h2>Top-Auswahl</h2>
        <p style="max-width: 400px; margin: 0 auto;">
            Unsere Top-Auswahl spezialisiert sich auf den momentanen Trend in der Uhrenwelt. Top-Marken finden Sie hier genauso wie trendige Style-Akzente für die nächste Veranstaltung.
        </p>
    </div>

    <div class="illustration-v2 margin-bottom-60">
        <div class="customNavigation margin-bottom-25">
            <a class="owl-btn prev rounded-x"><i class="fa fa-angle-left"></i></a>
            <a class="owl-btn next rounded-x"><i class="fa fa-angle-right"></i></a>
        </div>

        <ul class="list-inline owl-slider">
            @foreach($featuredProducts as $feature)
                <li class="item">
                    <div class="product-img">
                        <a href="{{url("shop/".$feature->gender."/v/".$feature->category."/".$feature->id."/".$feature->seo_slug)}}">
                            <img class="full-width img-responsive" id="catImgB" src="{{$feature->thumbnail}}_SL250_.jpg" alt="{{$feature->name}}"></a>

                        @if(strtotime($feature->created_at) - time() < 86400)
                        <div class="shop-rgba-dark-green rgba-banner">New</div>
                        @endif
                    </div>
                    <div class="product-description product-description-brd">
                        <div class="overflow-h margin-bottom-5">
                            <div class="pull-left">
                                <h4 class="title-price"><a href="{{url("shop/".$feature->gender."/v/".$feature->category."/".$feature->id."/".$feature->seo_slug)}}">{{$feature->name}}</a></h4>
                                <span class="gender text-uppercase">
                                    @if($feature->gender == 0)
                                        <span class="gender">Damen</span>
                                    @elseif($feature->gender == 1)
                                        <span class="gender">Herren</span>
                                    @elseif($feature->gender == 2)
                                        <span class="gender">Unisex</span>
                                    @endif

                                </span>
                                <span class="gender">{{$feature->category}}</span>
                            </div>
                            <div class="product-price">
                                <span class="title-price">€ {{number_format($feature->price/100,2, ",", ".")}}*</span>
                            </div>
                        </div>

                    </div>
                </li>
            @endforeach
        </ul>
    </div>
    <div class="row margin-bottom-50">

        <div class="col-md-4 md-margin-bottom-30">
            <div class="overflow-h">
                <a class="illustration-v3 illustration-men" href="{{url("shop/herren/find")}}">
					<span class="illustration-bg">
						<span class="illustration-ads">
							<span class="illustration-v3-category">
								<span class="product-category">Herren</span>
							</span>
						</span>
					</span>
                </a>
            </div>
        </div>

        <div class="col-md-4 md-margin-bottom-30">
            <div class="overflow-h">
                <a class="illustration-v3 illustration-women" href="{{url("shop/damen/find")}}">
					<span class="illustration-bg">
						<span class="illustration-ads">
							<span class="illustration-v3-category">
								<span class="product-category">Damen</span>
							</span>
						</span>
					</span>
                </a>
            </div>
        </div>

        <div class="col-md-4 md-margin-bottom-30">
            <div class="overflow-h">
                <a class="illustration-v3 illustration-unisex" href="{{url("shop/unisex/find")}}">
					<span class="illustration-bg">
						<span class="illustration-ads">
							<span class="illustration-v3-category">
								<span class="product-category">Unisex</span>
							</span>
						</span>
					</span>
                </a>
            </div>
        </div>

    </div>
    <div class="illustration-v2 margin-bottom-20">
        <div class="heading heading-v1 margin-bottom-40">
            <h2>Neue Produkte</h2>
            <p>
                Neue Uhrenmodelle
            </p>
        </div>
        <div class="customNavigation margin-bottom-25">
            <a class="owl-btn prev rounded-x"><i class="fa fa-angle-left"></i></a>
            <a class="owl-btn next rounded-x"><i class="fa fa-angle-right"></i></a>
        </div>
        <ul class="list-inline owl-slider">
            @foreach($latestProducts as $latest)
                <li class="item">
                    <div class="product-img">
                        <a href="{{url("shop/".$latest->gender."/v/".$latest->category."/".$latest->id."/".$latest->seo_slug)}}">
                            <img class="full-width img-responsive" id="catImgB" src="{{$latest->thumbnail}}_SL250_.jpg" alt="{{$latest->name}}">
                        </a>
                        @if(strtotime($latest->created_at) - time() < 86400)
                            <div class="shop-rgba-dark-green rgba-banner">New</div>
                        @endif
                    </div>
                    <div class="product-description product-description-brd">
                        <div class="overflow-h margin-bottom-5">
                            <div class="pull-left">
                                <h4 class="title-price"><a href="{{url("shop/".$latest->gender."/v/".$latest->category."/".$latest->id."/".$latest->seo_slug)}}">{{$latest->name}}</a></h4>
                                <span class="gender text-uppercase">
                                    @if($latest->gender == 0)
                                        <span class="gender">Damen</span>
                                    @elseif($latest->gender == 1)
                                        <span class="gender">Herren</span>
                                    @elseif($latest->gender == 2)
                                        <span class="gender">Unisex</span>
                                    @endif
                                </span>
                                <span class="gender">{{$latest->category}}</span>
                            </div>
                            <div class="product-price">
                                <span class="title-price">€ {{number_format($latest->price/100,2, ",", ".")}}*</span>
                            </div>
                        </div>

                    </div>
                </li>
            @endforeach
        </ul>

    </div>

</div>
<div class=" container-fluid txtMk" >
    <div class="col-md-6 " >
        <p>
        <h3>Uhren haben Geschichte</h3>
        Raus aus der Deckung und rein ins Vergnügen. Denn hier finden Sie nicht nur Top-Marken sondern auch Raritäten die es nicht überall gibt.
        Sei es Michael Kors, Cluse, oder Zeppelin hier bekommen Sie Ihre Uhr günstig online.
        Wir haben uns zum Ziel gesetzt unsere Angebote von Zeitmessern speziell den Bedürfnissen des Modebeussten aber auch des Funktionellen Typs anzupassen womit
        wir Ihnen eine große Palette von Uhren anbeiten können die für Qualität stehen. Ob Sie auf der Suche nach Automatikuhren, Quarzuhren oder Digitaluhren im Retro-Look
        sind spielt dabei keine Rolle, denn wir helfen Ihnen dabei Ihre neue Uhr zu finden, ohne lange Diskussion darüber zu halten.
        Achten Sie immer darauf: Uhren haben eine Geschichte welche Sie am Arm tragen, immer ablesen und anderen Menschen weitererzählen.

        </p>

    </div>
    <div class="col-md-6 " >
        <p >
        <h3>Zeitmesser aus Verwantwortung</h3>
        Pünktlichkeit, Genauigkeit und Zuverlässigkeit machen nicht nur einen Menschen aus, sondern auch einen guten Chronometer. Mit viel Fingerspitzengefühl wird eine Uhr zusammengestellt
        um Performance zu liefern, die über den Tag nicht verloren gehen darf und um diese Punkte in die Uhr einzuarbeiten. Hohe Genauigkeit sorgen für Pünktliche bei Terminen. Eine zuverlässige
        Uhr sorgt auch noch dann für Pünktlichekeit, wenn man sie wirklich nötig hat. Eine Uhr soll dies vereine, wie es jede Uhr hier macht, denn hier finden Sie Qualitätuhren die auch noch
        tickt Wenn Sie es nichtmehr tun.


        </p>

    </div>
</div>
@stop
@section('footer')
    <script src="{{asset("plugins/revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js")}}"></script>
    <script src="{{asset("plugins/revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js")}}"></script>
    <script src="{{asset("js/plugins/revolution-slider.js")}}"></script>
<script>
    jQuery(document).ready(function() {

    });

    $('document').ready(function(){
        RevolutionSlider.initRSfullWidth();

        ajaxuse = false;

    });
</script>
@stop