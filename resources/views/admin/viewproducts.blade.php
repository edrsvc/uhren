@extends('layouts.app')
@section("header")
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop
@section('content')
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left">View Products</h1>
            <ul class="pull-right breadcrumb">
                <li><a href="index.html">Admin</a></li>
                <li><a href="">Amazon</a></li>
                <li class="active">View Products</li>
            </ul>
        </div>
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <div class="container content">
       @include("layouts.navigation")

        <div class="col-md-9">
            @if(count($products)>0)
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>ASIN</th>


                        <th>Category</th>
                        <th>Price</th>
                        <th>Views</th>
                        <th>Options</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($products AS $prodcut)
                        <tr>
                            <td>{{$prodcut->id}}</td>

                            <td><small>{{$prodcut->asin}}</small></td>
                            @if($prodcut->category == 0)
                                <td>Digitaluhren</td>
                            @elseif($prodcut->category == 1)
                                <td>Automatikuhren</td>
                            @elseif($prodcut->category == 2)
                                <td>Quarzuhren</td>
                            @endif
                            <td>€ {{number_format($prodcut->price/100,2, ",", ".")}}*</td>
                            <td>{{$prodcut->views}}</td>

                            <td>
                                <button class="delete" value="{{$prodcut->id}}">Delete</button>

                                @if($prodcut->featured == NULL)
                                    <button class="feature"  value="{{$prodcut->id}}" setting="1">Featute</button>
                                @elseif($prodcut->featured == 1)
                                    <button class="feature"  value="{{$prodcut->id}}" setting="0">Unfeature</button>
                                @elseif($prodcut->featured == 0)
                                    <button class="feature"  value="{{$prodcut->id}}" setting="1">Featute</button>
                                @endif

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
            @else
                <div class="alert alert-info fade in">
                    Oh, there are no products yet. <a href="http://localhost:8000/admin/addproduct">Let's add some now!</a>
                </div>
            @endif
            {!! $products->render() !!}
        </div>

    </div>


    </div>
@endsection

@section("footer")
    <script>
        $('document'). ready( function(){
            ajaxuse = false;
            $(".feature").on("click", function () {
                featureitem = $(this);
                if(ajaxuse == true)
                {
                    return;
                } else {
                    ajaxuse = true;
                    $.ajax({
                        url: '{{url('admin/products/feature/')}}'+"/"+featureitem.val()+"/"+featureitem.attr("setting"),
                        type: 'POST',
                        data:{
                            '_token' : '{{csrf_token()}}'
                        },
                        success: function(result){
                            if(result == 1)
                            {
                                featureitem.attr("setting", "0");
                                featureitem.text("Unfeature")

                            }
                            else if(result == 0)
                            {
                                featureitem.attr("setting", "1")
                                featureitem.text("Feature")
                            }




                        }
                    });
                    ajaxuse = false;
                }

            });
            $(".delete").on("click", function () {
                delitem = $(this);
                if(ajaxuse == true)
                {
                    return;
                } else {
                    ajaxuse = true;
                    $.ajax({
                        url: '{{url('admin/products/delete/')}}'+"/"+$(this).val(),
                        type: 'POST',
                        data:{
                            '_token' : '{{csrf_token()}}'
                        },
                        success: function(result){
                                if(result == "Success")
                                {
                                    delitem.parent().parent().fadeOut( "slow" ).remove();
                                }
                                else
                                {
                                    alert('Error while trying to delete product<br>'.result);

                                }


                        }
                    });
                    ajaxuse = false;
                }
            });
        });
    </script>
@stop