@extends('layouts.app')
@section("header")
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop
@section('content')
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left">View Products</h1>
            <ul class="pull-right breadcrumb">
                <li><a href="index.html">Admin</a></li>
                <li><a href="">Blog</a></li>
                <li class="active">View Blogposts</li>
            </ul>
        </div>
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <div class="container content">
        @include("layouts.navigation")

        <div class="col-md-9">
            @if(count($articles)>0)
                <table class="table">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Titel</th>
                        <th>Category</th>
                        <th>Edit</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($articles AS $article)
                        <tr>
                            <td>{{$article->id}}</td>
                            <td>
                                {{$article->title}}
                            </td>
                            <td>
                                {{$article->category}}
                            </td>
                            <td>
                                <a href="#" class="editArticle" id="editArticle" articleId="{{$article->id}}">Edit Article</a>
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div id="editorContainer" hidden>
                    <button type="button" class="btn-u btn-u-default" id="backbutton">Back</button>
                    <div id="editor"></div>
                </div>
            @else
                <div class="alert alert-info fade in">
                    Oh, there are no products yet. <a href="http://localhost:8000/admin/addproduct">Let's add some now!</a>
                </div>
            @endif
            {!! $articles->render() !!}
        </div>

    </div>


    </div>
@endsection

@section("footer")
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

    <script >
        $('document'). ready( function(){

            $(".editArticle").on("click", function(){
                $("table").hide();
                $("#editor").load("{{url("admin/blog/update")}}" + "/"+ $(this).attr("articleId"));
                $("#editorContainer").show();

            });
            $("#backbutton").on("click", function(){
                $("#editor").html("");
                $("#editorContainer").hide();
                $("table").show();
            });



            ///=====
            ajaxuse = false;
            $(".feature").on("click", function () {
                featureitem = $(this);
                if(ajaxuse == true)
                {
                    return;
                } else {
                    ajaxuse = true;
                    $.ajax({
                        url: '{{url('admin/products/feature/')}}'+"/"+featureitem.val()+"/"+featureitem.attr("setting"),
                        type: 'POST',
                        data:{
                            '_token' : '{{csrf_token()}}'
                        },
                        success: function(result){
                            if(result == 1)
                            {
                                featureitem.attr("setting", "0");
                                featureitem.text("Unfeature")

                            }
                            else if(result == 0)
                            {
                                featureitem.attr("setting", "1")
                                featureitem.text("Feature")
                            }




                        }
                    });
                    ajaxuse = false;
                }

            });
            $(".delete").on("click", function () {
                delitem = $(this);
                if(ajaxuse == true)
                {
                    return;
                } else {
                    ajaxuse = true;
                    $.ajax({
                        url: '{{url('admin/products/delete/')}}'+"/"+$(this).val(),
                        type: 'POST',
                        data:{
                            '_token' : '{{csrf_token()}}'
                        },
                        success: function(result){
                            if(result == "Success")
                            {
                                delitem.parent().parent().fadeOut( "slow" ).remove();
                            }
                            else
                            {
                                alert('Error while trying to delete product<br>'.result);

                            }


                        }
                    });
                    ajaxuse = false;
                }
            });
        });
    </script>
@stop