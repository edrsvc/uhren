@extends('layouts.app')
@section("header")

@stop
@section('content')
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left">Meta</h1>
            <ul class="pull-right breadcrumb">
                <li><a href="index.html">Admin</a></li>
                <li><a href="">Site</a></li>
                <li class="active">Meta</li>
            </ul>
        </div>
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <div class="container content">
        @if(Session::has("notification"))
            <div class="alert alert-success fade in">
                {{Session::get("notification")}}
            </div>
        @endif

        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger fade in">
                    {{ $error }}
                </div>
            @endforeach
        @endif
            @include("layouts.navigation")


        <div class="col-md-9">
            {{ Form::open(array('url' => 'admin/site/meta', "class" => "sky-form", "method" => "PATCH" )) }}
            <header>Edit Site´s Meta informations</header>
            <fieldset>
                <section>
                    <label class="label">Title</label>
                    <label class="input">
                        <input type="text" name="title" value="{{$settings[0]->title}}">
                    </label>
                </section>

                <section>
                    <label class="label">Keywords</label>
                    <label class="input">
                        <input type="text" name="keywords" value="{{$settings[0]->keywords}}">
                    </label>
                </section>

                <section>
                    <label class="label">Copyright</label>
                    <label class="input">
                        <input type="text" name="copyright"  value="{{$settings[0]->copyright}}">
                    </label>
                </section>

                <section>
                    <label class="label">Description</label>
                    <label class="input">
                        <input type="text" name="description"  value="{{$settings[0]->description}}">
                    </label>
                </section>

                <section>
                    <label class="label">Revisit-After</label>
                    <label class="input">
                        <input type="text" name="revisit_after"  value="{{$settings[0]->revisit_after}}">
                    </label>
                </section>

            </fieldset>
            <footer>
                <button type="submit" class="btn-u">Submit</button>
                <button type="button" class="btn-u btn-u-default" onclick="window.history.back();">Back</button>
            </footer>
            {{ Form::close() }}
        </div>
    </div>


    </div>
@endsection

@section("footer")



    <link rel="stylesheet" href="{{asset("plugins/sky-forms-pro/skyforms/css/sky-forms.css")}}">
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="{{asset("css/sky-forms-ie8.css")}}">
    <![endif]-->


    <script src="{{asset("js/jquery.form.min.js")}}"></script>
    <script src="{{asset("js/jquery.validate.min.js")}}"></script>
    <script src="{{asset("js/jquery.maskedinput.min.js")}}"></script>
    <script src="{{asset("js/jquery.modal.js")}}"></script>
    <!--[if lt IE 10]>
    <script src="{{asset("js/jquery.placeholder.min.js")}}"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src="{{asset("js/sky-forms-ie8.js")}}"></script>
    <![endif]-->
@stop
