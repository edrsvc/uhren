@extends('layouts.app')
@section("header")

@stop
@section('content')
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left">Add Product</h1>
            <ul class="pull-right breadcrumb">
                <li><a href="index.html">Admin</a></li>
                <li><a href="">Blog</a></li>
                <li class="active">Add Blogpost</li>
            </ul>
        </div>
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <div class="container content">
        @if(Session::has("notification"))
            <div class="alert alert-success fade in">
                {{Session::get("notification")}}
            </div>
        @endif

        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger fade in">
                    {{ $error }}
                </div>
            @endforeach
        @endif

        @include("layouts.navigation")


        <div class="col-md-9">
            {{ Form::open(array('url' => 'admin/blog/add', "class" => "sky-form", 'method' => 'POST','files' => true)) }}
            <header>Add a new Blogpost</header>
            <fieldset>
                <section>
                    <label class="label">Titel</label>
                    <label class="input">
                        <input type="text" name="title" id="title" value="">
                    </label>
                </section>

                <section>
                    <label class="label">Seo-Slug</label>
                    <label class="input">
                        <input type="text" name="seo_slug" id="slug" value="">
                    </label>
                </section>

                <section>
                    <label class="label">Description</label>
                    <label class="input">
                        <input type="text" name="description" placeholder="about 166 characters long" >
                    </label>
                </section>

                <section>
                    <label class="label">Post-Image</label>
                        {{Form::file('image')}}
                </section>
                <section>
                    <label class="label">Image Organization</label>
                    <label class="input">
                        <input type="text" name="image_organization" placeholder="" >
                    </label>
                </section>
                <section>
                    <label class="label">Image Credit</label>
                    <label class="input">
                        <input type="text" name="image_credit" placeholder="" >
                    </label>
                </section>
                <section>
                    <label class="label">Image Caption</label>
                    <label class="input">
                        <input type="text" name="image_caption" placeholder="" >
                    </label>
                </section>

                <section>
                    <textarea rows="10" id="article" name="text"></textarea>
                </section>

                <section>
                    <label class="label">Keywords</label>
                    <label class="input">
                        <input type="text" name="keywords">
                    </label>
                </section>

                <section>
                    <label class="label">Category</label>
                    <label class="select state-success">
                        <select name="category" id="">
                            <option value="news">News</option>
                            <option value="uhren_test">Uhren-Test</option>
                        </select>
                    </label>
                </section>
            </fieldset>
            <footer>
                <button type="submit" class="btn-u">Submit</button>
                <button type="button" class="btn-u btn-u-default" onclick="window.history.back();">Back</button>
            </footer>
            {{ Form::close() }}
        </div>
    </div>


    </div>
@endsection
@section("footer")



    <link rel="stylesheet" href="{{asset("plugins/sky-forms-pro/skyforms/css/sky-forms.css")}}">
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="{{asset("css/sky-forms-ie8.css")}}">
    <![endif]-->


    <script src="{{asset("js/jquery.form.min.js")}}"></script>
    <script src="{{asset("js/jquery.validate.min.js")}}"></script>
    <script src="{{asset("js/jquery.maskedinput.min.js")}}"></script>
    <script src="{{asset("js/jquery.modal.js")}}"></script>
    <!--[if lt IE 10]>
    <script src="{{asset("js/jquery.placeholder.min.js")}}"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src="{{asset("js/sky-forms-ie8.js")}}"></script>
    <![endif]-->

    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

    <script>tinymce.init({ selector:'textarea',plugins: "image,media", });</script>


    <script>
        $('document'). ready( function(){
            $("#title").keypress(function(event)
            {
                if(event.charCode == 32)
                {
                    cur = $("#slug").attr("value");

                    $("#slug").attr("value", cur+"-");

                }
                else if( event.key == "Backspace")
                {
                    cur = $("#slug").attr("value").slice(0,-1);

                    $("#slug").attr("value", cur);
                }
                else if( event.key == "Tab")
                {
                    return;
                }
                else if(event.key.match(/[&\/\\#,+()$~%.'":*?<>{}]/g) !== null){
                    return;
                }
                else
                {
                    cur = $("#slug").attr("value");

                    $("#slug").attr("value", cur+event.key);
                }
            });



            $(".sky-form").validate({
                rules: {
                    title: {
                        required: true,

                    },
                    slug: "required",
                    description: {
                        required:true,
                        maxlength: 166
                    },
                    image_credit: "required",
                    image_caption:"required",
                    image_organization:"required",
                    category: "required",
                    article: "required"

                },
                submitHandler: function(form) {


                    // some other code
                    // maybe disabling submit button
                    // then:
                    $(form).submit();
                }
            });
        });
    </script>
@stop
