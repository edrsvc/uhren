@extends('layouts.app')
@section("header")

@stop
@section('content')
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left">General</h1>
            <ul class="pull-right breadcrumb">
                <li><a href="index.html">Admin</a></li>
                <li><a href="">Site</a></li>
                <li class="active">General</li>
            </ul>
        </div>
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <div class="container content">
        @if(Session::has("notification"))
            <div class="alert alert-success fade in">
                {{Session::get("notification")}}
            </div>
        @endif

        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger fade in">
                    {{ $error }}
                </div>
            @endforeach
        @endif
        @include("layouts.navigation")


        <div class="col-md-9">
            {{ Form::open(array('url' => 'admin/site/general', "class" => "sky-form", "method" => "PATCH" )) }}
            <header>Edit Site´s Meta informations</header>
            <fieldset>
                <section>
                    <label class="label">Google Analytics</label>
                    <label class="input">
                        <input type="text" name="google_analytics_code" value="{{$settings[0]->google_analytics_code}}">
                    </label>
                </section>

                <section>
                    <label class="label">Google Siteverification</label>
                    <label class="input">
                        <input type="text" name="google_site_verify_code" value="{{$settings[0]->google_site_verify_code}}">
                    </label>
                </section>

                <section>
                    <label class="label">Additional Javascript</label>
                    <label class="input">
                        <textarea name="additional_javascript" style="width: 100%" rows="8">{{$settings[0]->additional_javascript}}</textarea>
                    </label>
                </section>


            </fieldset>
            <footer>
                <button type="submit" class="btn-u">Submit</button>
                <button type="button" class="btn-u btn-u-default" onclick="window.history.back();">Back</button>
            </footer>
            {{ Form::close() }}
        </div>
    </div>


    </div>
@endsection

@section("footer")



    <link rel="stylesheet" href="{{asset("plugins/sky-forms-pro/skyforms/css/sky-forms.css")}}">
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="{{asset("css/sky-forms-ie8.css")}}">
    <![endif]-->


    <script src="{{asset("js/jquery.form.min.js")}}"></script>
    <script src="{{asset("js/jquery.validate.min.js")}}"></script>
    <script src="{{asset("js/jquery.maskedinput.min.js")}}"></script>
    <script src="{{asset("js/jquery.modal.js")}}"></script>
    <!--[if lt IE 10]>
    <script src="{{asset("js/jquery.placeholder.min.js")}}"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src="{{asset("js/sky-forms-ie8.js")}}"></script>
    <![endif]-->
@stop
