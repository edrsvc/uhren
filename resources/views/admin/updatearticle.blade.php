

{{ Form::open(array('url' => 'admin/blog/update/'.$article->id, "class" => "sky-form", 'method' => 'POST','files' => true))}}

<header>Edit Article</header>
<fieldset>
    <section>
        <label class="label">Titel</label>
        <label class="input">
            <input type="text" name="title" id="title"  value="{{$article->title}}">
        </label>
    </section>

    <section>
        <label class="label">Seo-Slug</label>
        <label class="input">
            <input type="text" name="seo_slug" id="slug" value="{{$article->seo_slug}}">
        </label>
    </section>

    <section>
        <label class="label">Description</label>
        <label class="input">
            <input type="text" name="description" placeholder="about 166 characters long" value="{{$article->description}}" >
        </label>
    </section>


    <section>
        <textarea rows="10" id="article" name="text">
            {{ $article->text }}
        </textarea>
    </section>

    <section>
        <label class="label">Keywords</label>
        <label class="input">
            <input type="text" name="keywords" value="{{$article->keywords}}">
        </label>
    </section>

    <section>
        <label class="label">Category</label>
        <label class="select state-success">
            <select name="category" id="">
                <option value="news">News</option>
                <option value="uhren_test">Uhren-Test</option>
            </select>
        </label>
    </section>
</fieldset>
<footer>
    <a class="btn submitButton" >Submit</a>
    <span id="feedback" hidden></span>

</footer>
{{ Form::close() }}


    <link rel="stylesheet" href="{{asset("plugins/sky-forms-pro/skyforms/css/sky-forms.css")}}">
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="{{asset("css/sky-forms-ie8.css")}}">
    <![endif]-->


    <script src="{{asset("js/jquery.form.min.js")}}"></script>
    <script src="{{asset("js/jquery.validate.min.js")}}"></script>
    <script src="{{asset("js/jquery.maskedinput.min.js")}}"></script>
    <script src="{{asset("js/jquery.modal.js")}}"></script>
    <!--[if lt IE 10]>
    <script src="{{asset("js/jquery.placeholder.min.js")}}"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src="{{asset("js/sky-forms-ie8.js")}}"></script>
    <![endif]-->





<script type="text/javascript">

    $('document'). ready( function(){


        $(".submitButton").on("click", function() {
            tinyMCE.triggerSave();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

           $.ajax({

               url: "{{url("admin/blog/update")}}/{{$article->id}}",
               type: 'POST',
               dataType: "text",
               processData: false,
               data: $(".sky-form").serialize(),

               success: function (result) {
                   if(result === "true"){
                       $("#feedback").text("Success").fadeIn("fast").fadeOut("slow");
                   } else {
                       $("#feedback").text("Error").fadeIn("fast").fadeOut("slow");
                   }
               }
           });
        });

        $("#title").keypress(function(event)
        {
            if(event.charCode == 32)
            {
                cur = $("#slug").attr("value");

                $("#slug").attr("value", cur+"-");

            }
            else if( event.key == "Backspace")
            {
                cur = $("#slug").attr("value").slice(0,-1);

                $("#slug").attr("value", cur);
            }
            else if( event.key == "Tab")
            {
                return;
            }
            else if(event.key.match(/[&\/\\#,+()$~%.'":*?<>{}]/g) !== null){
                return;
            }
            else
            {
                cur = $("#slug").attr("value");

                $("#slug").attr("value", cur+event.key);
            }
        });

        $(".sky-form").validate({
            rules: {
                title: {
                    required: true,

                },
                slug: "required",
                description: {
                    required:true,
                    maxlength: 166
                },
                category: "required",
                article: "required"

            },
            submitHandler: function(form) {


                // some other code
                // maybe disabling submit button
                // then:
                $(form).submit();
            }
        });
    });

</script>
<script type="text/javascript">
    tinymce.init({selector:'textarea',plugins: "image,media",  relative_urls :false,convert_urls : true, remove_script_host : false,});
    tinyMCE.activeEditor.dom.addClass(tinyMCE.activeEditor.dom.select('img'), 'img-responsive');
</script>