@extends('layouts.app')
@section("header")

@stop
@section('content')
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left">Add Product</h1>
            <ul class="pull-right breadcrumb">
                <li><a href="index.html">Admin</a></li>
                <li><a href="">Amazon</a></li>
                <li class="active">Add Product</li>
            </ul>
        </div>
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <div class="container content">
        @if(Session::has("notification"))
            <div class="alert alert-success fade in">
                {{Session::get("notification")}}
            </div>
        @endif

        @if (count($errors) > 0)
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger fade in">
                        {{ $error }}
                    </div>
                @endforeach
        @endif

            @include("layouts.navigation")


        <div class="col-md-9">
            {{ Form::open(array('url' => 'admin/products/add', "class" => "sky-form", 'method' => 'PUT')) }}
                <header>Add a new Product</header>
                <fieldset>
                    <section>
                        <label class="label">Asin</label>
                        <label class="input">
                            <input type="text" name="asin">
                        </label>
                    </section>
                    <section>
                        <label class="label">Geschlecht</label>
                        <label class="select state-success">
                            <select name="gender" id="">
                                <option value="damen">Damen</option>
                                <option value="herren">Herren</option>
                                <option value="unisex">Unisex</option>
                                <option value="haushaltsuhren">Haushaltsuhren</option>
                                <option value="zubeheor">Zubeheur</option>


                                <option value="aufbewahrung">Aufbewahrung</option>
                                <option value="batterien">Batterien</option>
                                <option value="reperatur">Reperatur</option>


                                <option value="u">Undefiniert</option>
                            </select>
                            <i></i>
                        </label>
                    </section>
                    <section>
                        <label class="label">Kategorie</label>
                        <label class="select state-success">
                            <select name="category" id="">
                                <option value="Digitaluhren">Digitaluhren</option>
                                <option value="Automatikuhren">Automatikuhren</option>
                                <option value="Quarzuhren">Quarzuhren</option>

                                <option value="Smartwatches">Smartwatches</option>
                                <option value="Kuckucksuhren">Kuckucksuhren</option>
                                <option value="Wanduhren">Wanduhren</option>
                                <option value="Pendeluhren">Pendeluhren</option>
                                <option value="Haushaltswecker">Haushaltswecker</option>
                                <option value="Kurzzeitwecker">Kurzzeitwecker</option>

                                <option value="Uhrenarmbaender">Uhrenarmbänder</option>




                                <option value="Reperatursets">Reperatursets</option>

                                <option value="Uhrenboxen">Uhrenboxen</option>
                                <option value="Uhrenbeweger">Uhrenbeweger</option>

                                <option value="Uhrenbatterien">uhrenbatterien</option>

                                <option value="">Keine</option>
                            </select>
                            <i></i>
                        </label>
                    </section>
                </fieldset>
                <footer>
                    <button type="submit" class="btn-u">Submit</button>
                    <button type="button" class="btn-u btn-u-default" onclick="window.history.back();">Back</button>
                </footer>
            {{ Form::close() }}
        </div>
    </div>


    </div>
@endsection
@section("footer")



    <link rel="stylesheet" href="{{asset("plugins/sky-forms-pro/skyforms/css/sky-forms.css")}}">
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="{{asset("css/sky-forms-ie8.css")}}">
    <![endif]-->


    <script src="{{asset("js/jquery.form.min.js")}}"></script>
    <script src="{{asset("js/jquery.validate.min.js")}}"></script>
    <script src="{{asset("js/jquery.maskedinput.min.js")}}"></script>
    <script src="{{asset("js/jquery.modal.js")}}"></script>
    <!--[if lt IE 10]>
    <script src="{{asset("js/jquery.placeholder.min.js")}}"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src="{{asset("js/sky-forms-ie8.js")}}"></script>
    <![endif]-->


    <script>
        $('document'). ready( function(){
            $(".sky-form").validate({
                rules: {
                    asin: {
                        required: true,
                        minlength: 10,
                        maxlength: 10

                    },
                    category: "required",
                    gender: "required"

                },
                submitHandler: function(form) {


                    // some other code
                    // maybe disabling submit button
                    // then:
                    $(form).submit();
                }
            });
        });
    </script>
@stop
