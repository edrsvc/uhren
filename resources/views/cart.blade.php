@extends('layouts.app')
@section("header")
    <link rel="stylesheet" href="{{asset("plugins/jquery-steps/css/custom-jquery.steps.css")}}">
@stop
@section('content')
    <!--=== Breadcrumbs v4 ===-->
    <div class="breadcrumbs-v4">
        <div class="container">
            <span class="page-name">Warenkorb</span>
            <h1>Check<span class="shop-green">out</span></h1>
            <ul class="breadcrumb-v4-in">
                <li><a href="{{url("/")}}">Startseite</a></li>
                <li class="active"><a href="">Warenkorb</a></li>
            </ul>
        </div><!--/end container-->
    </div>
    <!--=== End Breadcrumbs v4 ===-->
    <!--=== End Breadcrumbs ===-->
        <!--=== Content Medium Part ===-->
        <div class="content-md margin-bottom-30">
            <div class="container">
                <form class="shopping-cart" action="#">
                    <div>
                        <div class="header-tags">
                            <div class="overflow-h">
                                <h2>Ihr Warenkorb</h2>
                                <p>Einkauf nochmals Prüfen</p>
                            </div>
                        </div>
                        <section>
                            <div class="table-responsive">
                                @if(isset($products) && count($products) >0 )
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Produkt</th>
                                            <th>Preis</th>
                                            <th>Menge</th>
                                            <th>Summe</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($products as $product)



                                            <tr>
                                                <td class="product-in-table">
                                                    <img class="img-responsive" src="{{$product->thumbnail}}_SL200.jpg" alt="">
                                                    <div class="product-it-in"  style="max-width: 300px;">
                                                         <h3>
                                                             {{$product->name}}
                                                         </h3>
                                                    </div>
                                                </td>
                                                <td>EUR  <i id="price_total">€ {{number_format($product->price/100,2, ",", ".")}}*</i></td>
                                                <td>
                                                    <select class="form-control input-sm qtyChekc" id="qtySelect" asin="{{$product->asin}}">
                                                        @for($i = 1; $i <= 10;$i++)
                                                        <option value="{{$i}}">{{$i}}</option>
                                                            @endfor
                                                    </select>
                                                </td>
                                                <td class="shop-red">{{number_format(($product->price/100)*(\Session::get("shopping_cart")[$product->asin]["qty"]),2, ",", ".")}}* </td>
                                                <td>
                                                    <div  asin="{{$product->asin}}" id="cart_delete" class="close">×</div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @endif
                            </div>
                        </section>





                        <div class="coupon-code">
                            <div class="row">
                                <div class="col-sm-4 sm-margin-bottom-30">

                                </div>
                                <div class="col-sm-3 col-sm-offset-5">
                                    <ul class="list-inline total-result">


                                        <li class="divider"></li>
                                        <li class="total-price">
                                            <h4>Total:</h4>
                                            <div class="total-result-in">
                                                @if(isset($subtotal))
                                                    <span>€ {{number_format($subtotal/100,2, ".", ".")}}*</span>
                                                @endif

                                            </div>
                                        </li>
                                        <li class="divider"></li>
                                        <li>

                                            <a href="{{\Session::get("amz_pruchase_url")}}">
                                                <button type="button" id="redirectAMZ" class="btn-u btn-u-sea-shop">Kaufen</button>
                                            </a>

                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div><!--/end container-->
        </div>
        <!--=== End Content Medium Part ===-->




    </div>
@endsection
@section("footer")

    <script>
        $(document).ready(function () {
            ajaxuse = false;


            $(".qtyChekc").on("change", function () {

                if(ajaxuse == true)
                {
                    return;
                }
                else
                {
                    ajaxuse = true;
                    $.ajax({
                        url: '{{url('cart/add/')}}' + "/" + $(this).attr("asin") + "/" + $(this).val(),
                        type: 'POST',
                        async: false,
                        dataType: "",
                        data: {
                            '_token': '{{csrf_token()}}'
                        },
                        success: function (result) {
                            ajaxuse = false;
                            location.reload();
                        }
                    });
                    ajaxuse = false;
                }
            });

            $(".close").on("click", function () {
                if(ajaxuse == true)
                {
                    return;
                }
                else
                {
                    ajaxuse = true;
                    $.ajax({
                        url: '{{url('cart/delete/')}}' + "/" + $(this).attr("asin"),
                        type: 'POST',
                        async: false,
                        dataType: "",
                        data: {
                            '_token': '{{csrf_token()}}'
                        },
                        success: function (result) {
                            ajaxuse = false;
                            location.reload();
                        }
                    });
                    ajaxuse = false;

                }

            });
        });

    </script>

    <script src="{{asset("plugins/jquery-steps/build/jquery.steps.js")}}"></script>
    <script src="{{asset("plugins/sky-forms-pro/skyforms/js/jquery.validate.min.js")}}"></script>
    <script src="{{asset("js/plugins/stepWizard.js")}}"></script>
    <script src="{{asset("js/forms/product-quantity.js")}}"></script>
    <script>
        jQuery(document).ready(function() {
            App.init();
            App.initScrollBar();

            StyleSwitcher.initStyleSwitcher();
        });
    </script>
@stop




















