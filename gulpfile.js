var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.styles([
        'bootstrap.min.css',
        'custom.css',
        'font-awesome.min.css',
        'header-v5.css',
        'jquery.mCustomScrollbar.css',
        'line-icons.css',
        'orange.css',
        'owl.carousel.css',
        'shop.style.css',
        'animate.css',
        'footer-v4.css'
    ]);
});
